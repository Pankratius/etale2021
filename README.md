These are my (probably extremely erroneous and also very provisional) lecture notes for the course [_Étale Cohomology_][1] taught by Dr. A. Ivanov at the University of Bonn.
A compiled version can be found [here][2].


[1]: https://www.math.uni-bonn.de/people/ivanov/etale_cohomology/etale_cohomology.html
[2]: https://pankratius.gitlab.io/etale2021/etale2021.pdf
