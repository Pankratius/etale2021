\section{Algebraic Spaces}
\begin{defn}
\label{algspc:def-representable}
	Let $S$ be a scheme.
	\begin{enumerate}
		\item 
			A map of presheaves $\shf\to \shg$ on $\fppf{S}$ is \emph{representable} if for all $X\in \fppf{S}$ and all map of presheaves $h_X \to \shg$, the fiber product $h_X\times_{\shg}\shf$ is representable.
		\item
			Let $P$ be a property of morphisms of schemes, which stable under base change and fppf-local on the base.
			Let $\shf \to \shg$ be a map of presheaves.
			We say that this map has the property $P$, if for all $X\in \fppf{S}$ and all morphisms $h_X \to \shg$, the Yoneda-induced map $Z \to X$ satisfies $P$, where $Z$ is the scheme that represents $\shf \times_\shg X$ (c.f. \cref{algspc:representable} below).
	\end{enumerate}
\end{defn}

\begin{example}
	Let $T\to S$ be a fppf $S$-scheme.
	Then the representable functor $h_T$ is an algebraic space:
\end{example}

\begin{lem}
\label{algspc:representable}
	Let $S$ be a scheme and $\shf \in \preshcat_{\setcat}(\fppf{S})$.
	Then the following are equivalent:
	\begin{enumerate}
		\item 
			The diagonal $\shf \to \shf \times \shf$ is representable.
		\item
			For every $X\in \fppf{S}$, all morphisms $X\to \shf$ are representable.
		\item
			For all $X,Y\in \fppf{S}$, and every pair of morphisms $h_X \to \shf$, $h_Y \to \shf$, the fiber product $X\times_{\shf} Y$ is representable.
	\end{enumerate}
\end{lem}

\begin{defn}
	Let $S$ be a scheme.
	An \emph{algebraic space} over $S$ is a sheaf of sets $\shf \in \shcat_{\setcat}(\fppf{S})$ satisfying:
	\begin{enumerate}
		\item 
			The diagonal $\shf \to \shf \times \shf$ is representable.
		\item
			There exists a $S$-scheme $U \to S$ and a map $h_U \to \shf$ of sheaves of sets, that is surjective and étale.
	\end{enumerate}
	A \emph{morphism of algebraic spaces} is a map of sheaves $\shf \to \shg$.
\end{defn}

\begin{defn}
	Let $S$ be a scheme.
	An \emph{equivalence relation} on an $S$-scheme $X$ is a monomorphism $(\iota_1,\iota_2)\mc R\to X\times_S X$ of $S$-schemes, such that for every $T\to S$, the subset 
	\[
		R(T) \sse X(T)\times_{S(T)} X(T)
		\]
	is and equivalence relation in the usual sense.
	An \emph{étale equivalence relation} on $X$ is an equivalence relation $j\mc R\to X\times_X$ such that the projections $\pi_{i}\circ j \mc R\to X$ are étale for $i=1,2$.
\end{defn}

\begin{construction}
	Let $X\in \fppf{S}$ be a scheme, and $R$ an equivalence relation on $X$.
	Let $\tau \in \lset \text{fppf, ét, ...}\rset$.
	The associated \emph{quotient sheaf} is the sheafification of the $\tau$-presheaf $T\mapsto X(T)/\sim_T$, where $\sim_T$ is the equivalence relation on $X(T)$ induced by $R$.
\end{construction}

\begin{theorem}
\label{algspaces:quotient-thm}
	Let $S$ be a scheme.
	\begin{enumerate}
		\item
			If $\shf$ is an algebraic space over $S$ and $U$ a $S$-scheme, then $R\defined U\times_{\shf}U$ is an étale equivalence relation and $U/R\to \shf$ is an isomorphism of fppf-sheaves.
		\item
			If $U$ is a $S$-scheme and $R\to U\times_S U$ an étale equivalence relation, then the fppf-quotient sheaf $U/R$ is an algebraic space.
	\end{enumerate}
\end{theorem}

\needspace{5\baselineskip}
\begin{defn}
	\leavevmode
	\begin{enumerate}
		\item
			Let $\shf'\to \shf$ be a map of algebraic spaces.
			We say that it is an \emph{open} respectively \emph{closed} immersion if it is representable and an open respectively closed immersion in the sense of \cref{algspc:def-representable}.
		\item
			Let $\lset \shf_i \to \shf\rset_i$ be a collection of open immersion of algebraic spaces.
			We say that it is a \emph{Zariski covering} if the resulting map $\coprod_i \shf_i \to \shf$ os surjective as map of sheaves.
		\item
			Let $\shf$ be an algebraic space over $S$, with diagonal $\Delta\mc \shf\times_S \shf$.
			We say that $\shf$ is \emph{separated/locally separated/quasi-separated}, if $\Delta$ is a closed immersion/an open immersion/quasi-compact.
	\end{enumerate}
\end{defn}

\begin{example}
	Let\footnote{\coms In the lecture, we stated this example for the more general case of an arbitrary field with characteristic $\neq 0$, but I'm not sure if this always works.\come} $ k = \cc$, and set $S\defined \spec(k)$ as well as $U \defined \affa^1_k$.
	Consider the map 
	\[
		j\mc R \defined \affa^1_k \amalg \left(\affa_k^1\setminus \lset 0\rset \right)
		\to 
		U\times U
	\]
	which is given by $x\mapsto (x,x)$ in the first component and by $y \mapsto (y,-y)$ in the second.
	Precomposing with the projections gives étale maps (i.e. $x\mapsto x$ and $y \mapsto \pm y$).
	Since for every $k$-scheme $T$, the relation $x\sim y \Leftrightarrow x = \pm y$ is an equivalence relation on $\affa_k^1(T)\times_k \left(\affa^1_k\setminus\lset 0\rset \right) (T)$, we get that $R$ is an étale equivalence relation on $U$.
	According to \cref{algspaces:quotient-thm}, the fppf quotient sheaf $\shf\defined U/R$ is an algebraic space.
	It fits into the following cartesian diagram of sheaves on $\fppf{S}$:
	\[
		\begin{tikzcd}
		R
		\ar{r}[above]{j}
		\ar{d}
		\ar[phantom]{rd}{\lrcorner}
		&
		U\times_k U
		\ar{d}
		\\
		\shf
		\ar{r}[below]{\Delta}
		&
		\shf\times_k \shf
		\end{tikzcd}
	\]

	The image of $j$ is not open in $\affa_k^2$: Otherwise, there would be a non-trivial polynomial $f$ that vanishes outside of it. 
	But in this case, the polynomial $(x-y)(x+y)f$ vanishes everywhere, which would imply that $f$ is trivial.
	So the map $\Delta \mc \shf \to \shf \times_k \shf$ can't be an immersion, and hence $\shf$ is not locally separated, and in particular not represented by a scheme.	
\end{example}

\begin{example}
	Let $k\sse k'$ be a Galois extension of degree $2$, with Galois group given by $\lset 1,\tau\rset$.
	Set $S \defined \spec(k[t])$ and $U\defined \spec(k'[t])$.
	Then $U\times_S U = \Delta(U) \amalg \Delta'(U)$, where $\Delta' = (1,\tau)\mc U\to U\times_S U$.
	Similar to the first example, the scheme 
	\[
		R \defined \Delta(U) \amalg \Delta'\left(U\setminus \lset 0\rset \right)
	\]
	is an étale equivalence relation on $U$, and we set $X \defined U/R$.
	\coms This is not finished \come
	%TODO: finish example about algebraic space for deg 2 galois.
\end{example}

\begin{numtext}
	As a last theorem of this course, we motivate algebraic spaces as class of representing objects of sheaves on the étale site of a scheme.
	We will construct this scheme first constructing representing objects for some fiber products, and then some glueing.
\end{numtext}

\begin{theorem}
	Let $S$ be a scheme and $\shf\in \shcat_{\setcat}(\et{S})$.
	Then $\shf$ is representable by a a locally separated algebraic space $\wtilde{\shf}$, i.e. for every étale $S$-scheme, there is a natural isomorphism $\shf(U) \smalliso \hom_{\algspaces/S}(h_U,\wtilde{\shf})$ and the diagonal $\wtilde{\shf} \to \wtilde{\shf}\times \wtilde{\shf}$ is an immersion.
	Moreover, $\wtilde{\shf}$ is unique up to unique isomorphism.
\end{theorem}

\begin{lem}
	Let $U_1,U_2\to S$ be étale.
	Let $s_i\mc h_{U_i}\to \shf$ be morphisms of sheaves of sets on $\et{S}$.
	Then the resulting fiber product $U_1\times_{\shf} U_2$ is represented by an open subscheme of $U_1\times_S U_2$.
\end{lem}
\begin{proof}
	We claim that the representing object is given by 
	\[
		\bigcup_{f_1,f_2\mc V \to U_1 \times_{\shf} U_2}
		\im
		\left(
			V\to U_1 \times_{\shf} U_2 \to U_1\times_S U_2
		\right),
	\]
	where we take the union in $U_1\times_S U_2$.
	
\end{proof}
