\subsection{\v{C}ech cohomology}
\begin{construction}
  Let $X$ be a scheme and $U\to X$ be an étale cover\footnote{We don't loose generality here, since every étale cover $\lset U_i \to X\rset_{i\in I}$ gives rise to a ``1-morphism-cover'', by considering the induced map $\coprod_{i\in I}U_i \to X$.}.
  The various projection maps give rise to a simplicial object in $\et{X}$ associated to $U$, which we call the \emph{\v{C}ech nerve} of $U$:
  \[
  \chcomplex(U/X)
  \defined
  \left(
  \tikzcdset{arrow style=tikz, diagrams={>=stealth}}
  \begin{tikzcd}[column sep = small]
    \ldots
    \ar{r}
    &
    U\times_X U \times_X U
    \ar[shift left = 1.25]{r}
    \ar{r}
    \ar[shift right = 1.25]{r}
    &
    U\times_X U
    \ar[shift left = .55]{r}
    \ar[shift right = .55]{r}
    &
    U
  \end{tikzcd}
  \right).
  \]
  \glsadd{chech-nerve}
  By abuse we will also write $\chcomplex(U/X)$ for the associated chain complex.
  Let now $\shf\in \shetcat{X}$ be a sheaf on $\et{X}$.
  Applying $\shf$ to $\chcomplex(U/X)$ gives a complex
  \[
  \chcomplex(U/X,\shf)
  =
  \begin{tikzcd}[column sep = small]
    0
    \ar{r}
    &
    \shf(U)
    \ar{r}
    &
    \shf(U\times_X U)
    \ar{r}
    &
    \shf(U\times_X U\times_X U)
    \ar{r}
    &
    \ldots
  \end{tikzcd},
  \]
  which we call the \emph{\v{C}ech complex} of $\shf$ with respect to $U$.
  Its homology groups are denoted by $\chhlgy^n(U/X,\shf)$ and called the ($n$-th) \emph{\v{C}ech cohomology} of $\shf$ with respect to $U$:
  \[
  \chhlgy^n(U/X,\shf)
  \defined
  \hh^n\left(\chcomplex(U/X,\shf)\right).
  \]
  \glsadd{chech-complex}
  \glsadd{chech-cohomology}
\end{construction}

\begin{construction}
\label{cechcoh:cechconstruction}
\leavevmode
\begin{enumerate}
\item
  If $f\mc V\to U$ is a morphism of étale $X$-schemes, then we get an induced morphism $\check{f}\mc \chcomplex(U,\shf)\to \chcomplex(V,\shf)$, induced by the $n$-fold fiber products of the map.
  If $g\mc V\to U$ is a second morphism, then $\check{f}$ and $\check{g}$ are chain homotopic.
  In particular, they induce the same maps on cohomology.
  A proof can of this be found in \cite{milne}*{III.2.1}, and a detailed in \cite{frankeag2}*{Lemma 1.2.1}.
\item
  The \emph{(absolute) \v{C}ech complex} of a sheaf $\shf$ on $\et{X}$ is defined as
  \[
  \chcomplex(\et{X},\shf)
  \defined
  \colim_{\substack{U\to X\\\text{étale}}} \chcomplex(U/X,\shf)
  \]
  which is only well-defined up to homotopy.
  The \emph{\v{C}ech cohomology} of $\et{X}$ with values in $\shf$ is defined as
  \[
  \chhlgy^{n}(\et{X},\shf)
  \defined
  \chhlgy^{n}(\chcomplex(\et{X},\shf)),
  \]
  and by the above this is indeed well-defined\footnote{We should be able to refine the colimit to a colimit over a filtered index set, and then we should be fine.}.
  \item
  \label{cechcoh:cechconstruction:cechpresheaf}
  We also have a \emph{\v{C}ech-cohomology presheaf} in the following sense:
  For every $\shp\in \preshcat(\et{X})$, the assignemt
  \[
  \chhlgy^n(-,\shp)\mc
  \et{X}
  \to
  \abcat,
  ~~
  U
  \mapsto
  \chhlgy^n(U,\shf)
  \]
  is a presheaf on $\et{X}$, which we denote by $\cechsheaf(\shf)$.
  There is a canonical map $\shp \to \cechsheaf^0(\shp)$, whose kernel $\shp_0$ evaluates at an étale $U\to X$ as the group of sections that rstrict to zero on some convering of $U$.
  So this map is injective if and only if $\shp$ is separated.
  Furthermore, for any presheaf $\shp$, the presheaf $\cechsheaf(\shp)$ is itself separated, and
  \[
  \cechsheaf^0(\cechsheaf^0(\shp))
  \cong
  \shp\sh
  \]
  holds.
  \end{enumerate}
\end{construction}

\begin{rem}
  We've only introduced the \v{C}ech complex for sheaves, because that's we're ultimately interested in, but everything we've said so far works for presheaves on $\et{X}$ too.
\end{rem}

\begin{lem}
  Let $U\to X$ be étale.
  The functor $\chcomplex(U/X,-)\mc \preshcat(\et{X})\to \chaincomplexcat(\abcat)$ is exact and for every $\shf\in \shcat(\et{X})$, it holds that
  \[
  \chhlgy^0(U/X,\shf) = \chhlgy^0(\et{X},\shf) = \Gamma(U,\shf).
  \]
  This part could have also been concluded from \ref{cechcoh:cechconstruction:cechpresheaf} of \cref{cechcoh:cechconstruction}.
\end{lem}
\begin{proof}
  The first claim follows from the fact that exactness of a sequence of presheaves can be checked ``point-wise''.
  The second part comes from the exactness of
  \[
  \begin{tikzcd}[column sep = small, cramped]
    \shf(X)
    \ar{r}
    &
    \shf(U)
    \ar{r}
    &
    \shf(U\times_X U),
  \end{tikzcd}
  \]
  and the exactness of (filtered) colimits.
\end{proof}

\begin{prop}
\label{cechcoh:effaceable}
  Let $U\to X$ be an étale cover, and $\shi \in \preshcat(\et{X})$ a presheaf.
  Then
  \[
  \chhlgy^n(U/X,\shi)
  =
  \chhlgy^n(\et{X},\shi)
  =
  0\]
  holds for all $n \geq 1$.
\end{prop}
\begin{proof}
It's enough to show that $\chcomplex(U/X,\shi)$ is exact expect at zero.
For that, consider the functor
\[
\zz_U\mc \schcat_X \to \abcat,~
Y\mapsto \zz[\hom_X(-,U)]
\]
which assigns to each scheme $Y$ over $X$ the free abelian group on the set $\hom_X(Y,U)$.
Now the projections from the iterated fiber products induce a complex of sheaves on the étale site $\et{X}$ of the form
\[
\zz_U
\to
\zz_{U\times_X U}
\to
\zz_{U\times_X U \times_X U}
\to
\ldots,
\]
which gives rise to a complex of abelian groups of the form
\[
\hom(\zz_U,\shi)
\to
\hom(\zz_{U\times_X U},\shi)
\to
\hom(\zz_{U\times_X U \times_X U},\shi)
\to
\ldots,
\]
which is isomorphic to $\chcomplex(U/X,\shi)$.
Because applying $\hom(-,\shi)$ is exact ($\shi$ is an injective object), it suffices to show that
\[
\zz
\to
\zz_U
\to
\zz_{U\times_X U}
\to
\zz_{U\times_X U \times_X U}
\to
\ldots
\]
is exact.
We can reduce this to free abelian groups on iterated products of the same set, and in that case a contracting homotopy can be explicitly constructed.
\end{proof}
\begin{cor}
  If for all short-exact sequences
  \[
  \begin{tikzcd}[column sep = small, cramped]
    0
    \ar{r}
    &
    \shf'
    \ar{r}
    &
    \shf
    \ar{r}
    &
    \shf''
    \ar{r}
    &
    0
  \end{tikzcd}
  \]
  of sheaves on $\et{X}$ the induced sequence of complexes
  \[
  \begin{tikzcd}[column sep = small]
    0
    \ar{r}
    &
    \chcomplex(\et{X},\shf')
    \ar{r}
    &
    \chcomplex(\et{X},\shf)
    \ar{r}
    &
    \chcomplex(\et{X},\shf'')
    \ar{r}
    &
    0
  \end{tikzcd}
  \]
  is again (right)-exact, then \v{C}ech cohomology and ordinary cohomology on the étale site agree, i.e. for all $n\geq 0$ and $\shf\in \shetcat{X}$, we have natural isomorphisms
  \[
  \chhlgy^n(\et{X},\shf)
  \isomorphism
  \hh^n(\et{X},\shf).
  \]
  The same statement holds for any cover $U\to X$.
\end{cor}
\begin{proof}
  The above \cref{cechcoh:effaceable} implies in particular that the collection $\chhlgy^{\bullet}(\et{X},-)$ is effaceable (we're using that the inclusion $\shcat(\et{X})\hookrightarrow \preshcat(\et{X})$ preserves injective objects to apply the corollary to the categories of sheaves on $\et{X}$).
  If the assumption of the corrollary holds, then they define a cohomological $\delta$-functor $\shcat(X)\to \abcat$, and by the uniqueness of them, they have to be isomorphic to $\hh^{\bullet}(\et{X},-)$ (since they're effaceable which implies that they're universal too).
\end{proof}

\begin{lem}
\label{cechcoh:cech-derived-as-presheaves}
  Let $U\to X$ be étale.
  As functors of presheaves, the higher \v{C}ech cohomology functors are the right-derived functors of $\chhlgy^0(U/X,-)$, i.e. there are natural isomorphisms
  \[
  \chhlgy^n(U/X,-)
  \isomorphism
  \rderived^n\chhlgy^0(U/X,-)
  \]
  for all $n \geq 0$.
\end{lem}
\begin{proof}
  I'll prbly add this at some later point, for now, the reference is \cite{stacks}*{\stackstag{03AU}}.
\end{proof}
\begin{rem}
  The same statement is wrong for sheaves, since it would imply that \v{C}ech cohomology always computes étale cohomology (\coms but I haven't thought about a counterexample yet\come).
\end{rem}

\begin{prop}
\label{cechcoh:chech-to-derived-spectral-sequence}
  Let $X$ be a scheme and $U\to X$ an étale covering.
  \begin{enumerate}
  \item
  \label{cechcoh:chech-to-derived-spectral-sequence:ordinary}
  For every $\shf\in \shetcat{X}$, there is a spectral sequence
  \[
  E_2^{p,q} = \chhlgy^p(U,\underline{\hh}^q(\shf))
  \Rightarrow
  \hh^{p+q}(\et{U},\shf).
  \]
  \item
  \label{cechcoh:chech-to-derived-spectral-sequence:cech-presheaf}
  The same holds for the \v{C}ech presheaf, i.e. there is a spectral sequence
  \[
  E_2^{p,q}
  =
  \cechsheaf^p(\underline{\hh}^q(\shf))
  \Rightarrow
  \underline{\hh}^{p+q}(\shf).
  \]
  \end{enumerate}
\end{prop}
\begin{rem}
\label{cechcoh:cech-to-derived:for-covers}
	Passing to covers, \ref{cechcoh:chech-to-derived-spectral-sequence:ordinary} gives, 
for any $U\to X$ étale and $\mathcal{U} = \lset U_i \to U\rset_i$ an étale cover of $U$, a spectral sequence 
	\[
		E_2^{r,s} 
		= 
		\chhlgy^r(\mathcal{U}/U,\underline{H}^s(\shf))
		\Rightarrow
		\hh(\et{U},\shf).
	\]
\end{rem}
\begin{proof}
  The starting point of this proof is the observation that the diagram
  \[
  \begin{tikzcd}
    \shetcat{X}
    \ar{r}[above]{\iota}
    \ar{rd}[below left]{\hh^0(\et{U},-)}
    &
    \preshcat(\et{X})
    \ar{d}[right]{\chhlgy^0(U/X,-)}
    \\
    &
    \abcat
  \end{tikzcd}
  \]
  commutes.
  Since we're dealing with compositions of derived functors, we want to use the Grothendieck spectral sequence from \cref{cohomology:grothendieck-spec-seq}.
  First of all, we recall that the derived functors of $\chhlgy^0(U,-)$ (as functors from the category presheaves) are precisely given by the higher \v{C}ech cohomology groups (\cref{cechcoh:effaceable}).
  And the condition \ref{cohomology:grothendieck-spec-seq:prelima} is indeed satisfied --- the inclusion $\shetcat{X}\hookrightarrow\preshcat(\et{X})$ preserves injective objects, and we've seen that $\hh^0(U,-)$ is effaceable (\cref{cechcoh:effaceable}).
  And that's it for (i).
  For (ii), all the statements we made in the proof of (i) hold in fact for all étale coverings $U\to X$, and so the hold for the \v{C}ech presheaves too.
\end{proof}

\begin{prop}
\label{cechcoh:cechpresheaf-of-higher-vanishes}
  Let $X$ be a scheme and $U\to X$ étale.
  Then $\chhlgy^0(U,\underline{\hh}^q(\shf)) = 0$ holds for $q\geq 1$.
\end{prop}
\begin{proof}
  Let $\shf\to \shi^{\bullet}$ be an injective resolution of $\shf$ by sheaves.
  Then by construction, $\underline{\hh}^q(\shf)$ is the $q$-th cohomology presheaf of the complex $\iota(\shi^{\bullet})$.
  Now sheafification is an exat functor, and hence $\left(\underline{\hh}^q(\shf)\right)\sh$ is the $q$-th cohomology presheaf of the sheaf $(\iota(\shi^{\bullet}))\sh = \shi^{\bullet}$, and this vanishes for $q>0$, since $\shi^{\bullet}$ is exact.
  Now by \cref{cechcoh:cechconstruction},\ref{cechcoh:cechconstruction:cechpresheaf}, we have that $\cechsheaf^0(\shf)$ is a sub-presheaf of the sheafification $\underline{\hh}^q(\shf)\sh$ and so $\chhlgy^q(U,\shf) = 0$ for all $U\to X$ étale follows.
\end{proof}

\begin{cor}
\label{cechcoh:first-cohs-agree}
  Let $\shf\in \shetcat{X}$ and $U\to X$ be an étale cover.
  Then there are isomorphism $\hh^0(U,\shf)\cong \chhlgy^0(U,\shf)$ and
  \[
  \hh^1(\et{U},\shf)\cong \chhlgy^1(U,\shf).
  \]
  Furthermore, there is an exact sequence:
  \[
  \begin{tikzcd}[column sep = small]
  0
  \ar{r}
  &
  \chhlgy^2(U,\shf)
  \ar{r}
  &
  \hh^2(\et{U},\shf)
  \ar{r}
  &
  \chhlgy^1(U,\underline{\hh}^1(\shf))
  \ar{r}
  &
  \chhlgy^3(U,\shf)
  \ar{r}
  &
  \hh^3(\et{U},\shf)
  \end{tikzcd}
  \]
\end{cor}
We will use the spectral sequence from \cref{cechcoh:chech-to-derived-spectral-sequence} for the proof.
Since this is new territory for me, let me spell some of the details out that we'll be using:
\begin{lem}
\label{cechcoh:spectral-sequences-in-low-degrees}
  Let $E_2^{p,q}\Rightarrow E^n$ be a spectral sequence.
  Then the following hold:
  \begin{enumerate}
    	\item
    	\label{cechcoh:spectral-sequences-in-low-degree-twoses}
	    	$E_2^{0,0} = E_{\infty}^{0,0} = E^0$.
	\item
	\label{cechcoh:spectral-sequences-in-low-degrees-ses1}
    The following sequence is exact:
    \[
    \begin{tikzcd}[column sep = small, cramped]
      0
      \ar{r}
      &
      E_2^{1,0}
      \ar{r}
      &
      E^1
      \ar{r}
      &
      E_2^{0,1}
      \ar{r}[above]{d_2^{0,1}}
      &
      E_2^{2,0}
    \end{tikzcd}
    \]
	\item
	\label{cechcoh:spectral-sequences-in-low-degrees-ses2}

    In fact, this sequence can be extended to the right to give an exact sequence:
    \[
    \begin{tikzcd}[column sep = small, cramped]
      0
      \ar{r}
      &
      E_2^{1,0}
      \ar{r}
      &
      E^1
      \ar{r}
      &
      E_2^{0,1}
      \ar{r}[above]{d_2^{0,1}}
      &
      E_2^{2,0}
      \ar{r}
      &
      E_1^2
      \ar{r}
      &
      E_2^{1,1}
      \ar{r}
      &
      E_2^{3,0}
    \end{tikzcd},
    \]
    where $E_1^2 \defined \ker(E_2\to E_2^{0,2})$.
  \end{enumerate}
\end{lem}
\needspace{3\baselineskip}
\begin{proof}[Proof of \cref{cechcoh:spectral-sequences-in-low-degrees}]
\leavevmode
\begin{enumerate}
  \item
  The first equality should realy be the definition of $E_{\infty}^{0,0}$; and the second one should be the definition of $E^{0}$ (\coms but I'm not sure if i'm missing something here\come).
  \item
  As part of the spectral sequence, we have a filtration
  \[
  E_1 \subseteq E_1^1 \sse 0
  \]
  such that $E_1^1 = E_{\infty}^{1,0}$.
  To describe $E_{\infty}^{1,0}$, we need to find a $r_0$ such that for all $r\geq r_0$ both $d_r^{1,0}$ and $d_r^{1-r,0}$ vanish --- we can take $r_0 = 2$ (since there're no ``strictly horizontal'' arrows on the $E_2$-page).
  So $E_1^1 = E_{\infty}^{1,0} = E_{2}^{1,0}$.
  Similarly, we have $E_1/E_1^1 = E_{\infty}^{0,1}$ and for the latter we have
  \[
  E_{\infty}^{0,1} = E_{3}^{0,1} = \ker(d_2^{0,1}).
  \]
  So the short-exact sequence associated to the filtration above can be extended to give
  \[
  \begin{tikzcd}[column sep = small, cramped]
    0
    \ar{r}
    &
    E_1^1
    \ar[hook]{r}
    &
    E^1
    \ar{r}
    &
    E^1/E^1_1
    \ar{r}[above]{d_2^{0,1}}
    &
    E_2^{2,0}
  \end{tikzcd}.
  \]
  Inserting all the identifactions from above gives the result.
  \item
  This should be similar to (ii), but take longer.
\end{enumerate}

\end{proof}
\begin{proof}[Proof of \cref{cechcoh:first-cohs-agree}]
  For the spectral sequence from \cref{cechcoh:chech-to-derived-spectral-sequence}, \ref{cechcoh:chech-to-derived-spectral-sequence:ordinary}, we have
  $E_2^{1,0} = \chhlgy^1(U,\underline{\hh^0}(\shf))$, $E_1 = \hh^1(\et{U},\shf)$; as well as $E_2^{0,1} = \chhlgy^0(U,\underline{\hh}(\shf)) = 0$, by \cref{cechcoh:cechpresheaf-of-higher-vanishes}.
  So using \cref{cechcoh:spectral-sequences-in-low-degrees},\ref{cechcoh:spectral-sequences-in-low-degree-twoses} this gives
  \[
  \chhlgy^1(U,\shf)
  =
  E_2^{1,0}
  \cong
  E_1
  =
  \hh^1(\et{U},\shf).
  \]
\end{proof}

\begin{cor}
\label{cechcoh:flasque-on-cech}
  Let $\shf\in \shetcat{X}$.
  Then ``being flasque'' can be checked via \v{C}ech cohomology, i.e. the following are equivalent:
  \begin{enumerate}
    \item
    \label{cechcoh:flasque-on-cech:flasquecond}
    $\shf$ is flasque, i.e. for all $U\to X$ étale, $\hh^n(U,\shf) = 0$ holds for all $n\geq 1$.
    \item
    \label{cechcoh:flasque-on-cech:cechcond}
    For all $U\to X$ étale, $\chhlgy^n(U,\shf) = 0$ holds for all $n\geq 1$.
  \end{enumerate}
\end{cor}

\begin{proof}
  For the implication \ref{cechcoh:flasque-on-cech:flasquecond} $\Rightarrow$ \ref{cechcoh:flasque-on-cech:cechcond}, we note that $\shf$ being flasque is equivalent to $\underline{\hh}^i(\shf) = 0$ for $i\geq 1$.
  In particular, the $E_2$-page of \ref{cechcoh:chech-to-derived-spectral-sequence:cech-presheaf} is zero except on the $p$-axis:
  \par
  \begin{center}
  \begin{tikzpicture}
    \matrix (m) [matrix of math nodes,
      nodes in empty cells,nodes={minimum width=5ex,
      minimum height=5ex,outer sep=-5pt},
      column sep=1ex,row sep=1ex]{
                  &      &     &     & \\
            2     &  0    &  0   &   0  & \\
            1     &  0 &  0  & 0 & \\
            0     &  \chhlgy^0(\shf)  & \chhlgy^1(\shf) &  \chhlgy^2(\shf)  & \\
      \quad\strut &   0  &  1  &  2  & \strut \\};
  \draw[thick] (m-1-1.east) -- (m-5-1.east) ;
  \draw[thick] (m-5-1.north) -- (m-5-5.north) ;
  \end{tikzpicture}
  \end{center}
  This implies $\chhlgy^n(\shf) = \underline{\hh}^n(\shf)$, as in the filtration of $E^n = \underline{\hh}^n(\shf)$,
  \[
  E^n \supseteq E^n_1 \supseteq E^n_2 \supseteq \ldots \supseteq E_n^n = E_{\infty}^{n,0}
  \]
  all but the last quotient are zero (since $E_{\infty}^{p,n-p} = 0$ for $p\geq 1$).
  But by assumption, $\shf$ is flasque, and so we conclude $\chhlgy^n(\shf) = \underline{\hh}^n(\shf) = 0$ for all $n\geq 1$.
  \par
  For the implication \ref{cechcoh:flasque-on-cech:cechcond} $\Rightarrow$ \ref{cechcoh:flasque-on-cech:flasquecond}, things are bit more subtle.
  Here, the $E_2$-page looks like:
  \par
  \begin{center}
  \begin{tikzpicture}
    \matrix (m) [matrix of math nodes,
      nodes in empty cells,nodes={minimum width=5ex,
      minimum height=5ex,outer sep=-5pt},
      column sep=1ex,row sep=1ex]{
                  &      &     &     &  &\\
            3     & 0& \chhlgy^1(\underline{\hh}^3(\shf))    & \chhlgy^2(\underline{\hh}^3(\shf))     & \chhlgy^3(\underline{\hh}^3(\shf))    & \\
            2     & 0& \chhlgy^1(\underline{\hh}^2(\shf))    & \chhlgy^2(\underline{\hh}^2(\shf))     & \chhlgy^3(\underline{\hh}^2(\shf))    & \\
            1     &  0 &  0  & 0 &  &\\
            0     &  \chhlgy^0(\shf)  & \chhlgy^1(\shf) &  \chhlgy^2(\shf)  & \chhlgy^3(\shf) &\\
      \quad\strut &   0  &  1  &  2  & 3 &\strut \\};
  \draw[thick] (m-1-1.east) -- (m-6-1.east) ;
  \draw[thick] (m-6-1.north) -- (m-6-6.north) ;
  \end{tikzpicture}
  \end{center}
  Here, for $p+q = 2$, we have $E_2^{p,q} = E_{\infty}^{p,q}$ (because this is the diagonal with all zeros except the last term), and hence $\underline{\hh}^2(\shf) = \chhlgy^2(\shf) = 0$, where the last equality holds by assumption.
  So the line $q = 2$ vanishes, and we get $E_2^{p,q} = E_{\infty}^{p,q}$ for $p+q = 3$. Continuing inductively, the claim follows.
\end{proof}

\begin{theorem}[Artin]
  Let $X$ be a noetherian scheme, such that every finite subset of $X$ is contained in an open affine set.
  Then for every $\shf\in \shetcat{X}$, \v{C}ech and ordinary étale cohomology agree:
  \[
  \chhlgy^{\bullet}(\et{X},\shf)
  =
  \hh^{\bullet}(\et{X},\shf).
  \]
\end{theorem}
We will not prove this theorem (and I'm not sure if we even need the spectral sequence results for that); a proof can be found in \cite{milne}*{III.2.17}.
However, we mentioned one lemma towards the proof in the lecture:
\begin{lem}
  Let $X$ be as in the theorem above, and $U\to X$ étale and of finite type.
  Let $(\overline{x}) = \left(\overline{x_1},\ldots,\overline{x_r}\right)$ be a finite set of geometric points of $X$, and write
  \[
  X_{\overline{x}}
  \defined
  \spec(\oo_{\overline{x_1}}\shnsl)
  \times
  \ldots
  \spec(\oo_{\overline{x_r}}\shnsl)
  \]
  Let $W\to U^n\times X_{\overline{x}}$ be an étale and surjective morphism.
  Then there exists a morphism $U'\to U$ that is étale and surjective, such that the induced morphism $U'\times X_{\overline{x}}\to U^n\times X_{\overline{x}}$ factors over $W$:
  \[
  \begin{tikzcd}
    U'^n\times X_{\overline{x}}
    \ar[dashed]{r}[above]{\exists}
    \ar[bend left = 30]{rr}
    &
    W
    \ar{r}
    &
    U^n\times X_{\overline{x}}
  \end{tikzcd}
  \]
\end{lem}

\subsubsection*{Étale cohomology of inverse limits}
\begin{numtext}
  We now want to use the methods of \v{C}ech cohomology to prove \cref{cohomology:commutes-with-inverse-limits}.
  Let us first spell out the setup more precisely:
  Let $I$ be a directed set, and $(X_i,f_{i',i})$ an inverse system of schemes over $I$, i.e. the $f_{i',i}$ are morphisms $f_{i',i}\mc X_i' \to X_{i}$ for $i'>i$, that are compatible in the sense that the diagram
  \[
  \begin{tikzcd}
    X_{i''}
    \ar{rr}[above]{f_{i'',i}}
    \ar{rd}[below left]{f_{i'',i'}}
    &
    &
    X_{i}
    \\
    &
    X_{i'}
    \ar{ur}[below right]{f_{i',i}}
    &
  \end{tikzcd}
  \]
  commutes for all $i''>i'>i$.
  We assume that all the transition maps $f_{i',i}$ are affine maps.
  \par
  Now a \emph{system of sheaves} on $(X_i,f_{i',i})$ is a collection $(\shf_i,\pphi_{i',i})$, where each $\shf_i\in \shcat(\etindex{X}{i})$ is a sheaf and for $i'\geq i$, $\pphi_{i',i}$ is a map of sheaves $\inverseimage{f_{i',i}}\shf_i \to \shf_{i'}$ on $\shcat(\etindex{X}{i'})$.
  We require these maps to be compatible in the sense that the diagram
  \[
  \begin{tikzcd}
  \inverseimage{f_{i'',i}}
  \shf_i
  \ar{rr}[above]{\pphi_{i'',i}}
  \ar{rd}[below left]{\inverseimage{f}_{i'',i'}\pphi_{i',i}}
  &
  &
  \shf_{i''}
  \\
  &
  \inverseimage{f_{i'',i'}}
  \shf_{i'}
  \ar{ur}[below right]{\pphi_{i'',i'}}
  \end{tikzcd}
  \]
  is required to commute for all $i''\geq i' \geq i$.
  \par
  Write $f_i\mc X\to X_i$ for the canonical projections.
  Then the morphisms
  \[
  \inverseimage{f_i}\shf_i
  =
  \inverseimage{f_{i'}}\inverseimage{f_{i',i}}\shf_i
  \xrightarrow{\inverseimage{f_{i'}}\pphi_{i',i}}
  \inverseimage{f_{i'}}\shf_{i'}
  \]
  turn $(\inverseimage{f_i}\shf)$ into a system of sheaves on $\et{X}$, in the sense that for all $i''\geq i' \geq i$, the diagram
  \[
  \begin{tikzcd}
    \inverseimage{f_i}\shf_i
    \ar{rr}[above]{\alpha_{i'',i}}
    \ar{rd}[below left]{\alpha_{i',i}}
    &
    &
    \inverseimage{f_{i''}}\shf_{i''}
    \\
    &
    \inverseimage{f_{i'}}\shf_{i'}
    \ar{ur}[below right]{\alpha_{i'',i'}}
    &
  \end{tikzcd}
  \]
  commutes.
\end{numtext}

\subsubsection*{Mayer-Vietoris sequence}
\begin{prop}
	Let $X = U \cup V$ be a Zarsiki-open covering of a scheme $X$.
	For any sheaf $\shf \in \shetcat{X}$, there is a functorial long-exact sequence:
	\[
		\begin{tikzcd}[column sep = small]
		0
		\ar{r}
		&
		\hh^0(\et{X},\shf)
		\ar{r}
		&
		\hh^0(\et{X},\shf)
		\oplus 
		\hh^0(\et{V},\shf)
		\ar[draw = none]{d}[name = X, anchor = center]{}
		\ar{r}
		&
		\hh^0(\et{\left(U\cap V\right)},\shf)
		\ar[rounded corners,
            to path={ -- ([xshift=2ex]\tikztostart.east)
                      |- (X.center) \tikztonodes
                      -| ([xshift=-2ex]\tikztotarget.west)
                      -- (\tikztotarget)}]{dll}[at end]{}
		&
		\\
		&
		\hh^1(\et{X},\shf)
		\ar{r}
		&
		\hh^1(\et{X},\shf)
		\oplus 
		\hh^1(\et{V},\shf)
		\ar{r}
		&
		\hh^1(\et{\left(U\cap V\right)},\shf)
		\ar{r}
		&
		\ldots
		\end{tikzcd}
	\]
\end{prop}

\begin{proof}
	Since we're working with open immersion, we can use the following general fact about \v{C}ech cohomology:
	\begin{fact}
		Let $T$ be a topological space, and $T = U_1 \cup U_2$ and open cover.
		Let $\shg$ be a sheaf of abelian groups on $T$.
		Then the sequence 
		\[
			\begin{tikzcd}[column sep = small, cramped]
			0
			\ar{r}
			&
			\shg(X)
			\ar{r}
			&
			\shg(U_1)
			\oplus
			\shg(U_2)
			\ar{r}
			&
			\shg(U_1 \cap U_2)
			\end{tikzcd}
		\]
		is exact.
		The right-most cohomology is given by $\chhlgy^1(\mathfrak{U},\shg)$, where $\mathfrak{U} = U_1 \cup U_2$ is the given cover.
	\end{fact}
	Let now $\shf \to \shi^{\bullet}$ be an injective resolution of $\shf$.
	By \cref{cechcoh:flasque-on-cech}, we have that $\chhlgy^1(\mathfrak{U},\shi_k) = 0$ for all injective sheaves $\shi_k$ of the injective resolution.
	So the sequence of complexes
	\[
		\begin{tikzcd}[column sep = small, cramped]
		0
		\ar{r}
		&
		\shi^{\bullet}(X)
		\ar{r}
		&
		\shi^{\bullet}(U)
		\oplus
		\shi^{\bullet}(V)
		\ar{r}
		&
		\shi^{\bullet}(U \cap V)
		\ar{r}
		&
		0
		\end{tikzcd}
	\]
	is exact.
	Taking the associated long-exact cohomology sequence gives the Mayer-Vietoris sequence, and the naturality of the construction.
\end{proof}

\subsubsection*{$\chhlgy^1$ and torsors}

\begin{defn}
	Let $\tcat$ be a site, and $\shg$ a sheaf of groups on $\tcat$.
	A \emph{$\shg$-torsor} is a sheaf of sets $\shs$ on $\tcat$, together with a morphism $\alpha \mc \shg \times \shs \to \shs$ of sheaves of sets that satisfies the usual conditions for a group action\footnote{For example, $\shg(U) \times \shs(U) \to \shs(U)$ is a group action for every $U \in \tcat$}, such that the following two conditions are satisfied:
	\begin{enumerate}
		\item
			For all $U \in \tcat$ with $\shs(U) \neq \emptyset$, the action $\shg(U) \times \shs(U) \to \shs(U)$ is simply transitive.
		\item
			For any $U \in \tcat$, there is a covering $\lset U_i \to U\rset$ such that $\shs(U_i)$ is non-empty for all $i$.
	\end{enumerate}
	A \emph{morphism of $\shg$-torsors} is a morphism $\tau\mc \shs \to \shs'$ of sheaves of sets that is compatible with the $\shg$-action in the sense that the diagram
	\begin{equation*}
		\begin{tikzcd}
		\shg \times \shs
		\ar{r}[above]{(\id,\tau)}
		\ar{d}[left]{\alpha}
		&
		\shg \times \shs'
		\ar{d}[right]{\alpha'}
		\\
		\shs
		\ar{r}[below]{\tau}
		&
		\shs'
		\end{tikzcd}
	\end{equation*}
	is required to be commutative.
	In that way, we obtain a \emph{category of $\shg$-torsors}, which we denote by $\shg\torscat$.
	A $\shg$-torsor $\shs$ is called \emph{trivial} if there is an isomorphism of $\shg$-torsors $\shs \smalliso \shg$ of $\shg$-torsors (were we regard $\shg$ as $\shg$-torsor via left-multiplication).
\end{defn}

\begin{lem}
	Assume that we are in the above setup.
	\begin{enumerate}
		\item
			A $\shg$-torsor $\shs$ is trivial if and only if $\lim_{U \in \tcat} \shs(U) \neq \emptyset$.
		\item
			The category of $\shg$-torsors on $\tcat$ is a groupoid.
	\end{enumerate}
\end{lem}

\begin{prop}
	Let $\tcat$ be a site and $\shg$ a sheaf of abelian groups on $\tcat$.
	Then there is a natural bijection:
	\[
		\pi_0(\shg\torscat) \isomorphism \chhlgy^1(\tcat,\shg)
	\]
\end{prop}

\begin{rem}
	In light of \cref{cechcoh:first-cohs-agree}, this is actually also a statement about the first étale cohomology of $X$, i.e. for any commutative group scheme $\shg$ on $X$, we have a bijection between the set of isomorphism classes of $\shg$-torsors and the first étale cohomology of $X$ with coefficients in $\shg$.
	Furthermore, the set $\pi_0(\shg\torscat)$ can be endowed with a ``natural'' group structure, which is compatible with that isomorphism (c.f. \cite{frankenotes}*{Rem. 2.3.10}).
\end{rem}
