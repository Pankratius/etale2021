\subsection{Cohomological dimension}
\begin{defn}
  Let $\tcat$ be a site and $\ell$ a prime number.
  \begin{enumerate}
    \item
    A sheaf $\shf$ on $\tcat$ is \emph{torsion} respectively \emph{$\ell$-torsion} if for all $U\in \obj(\tcat)$, the abelian group $\shf(U)$ is torsion respectively $\ell$-torsion.
    \item
    The \emph{$\ell$-cohomological dimension} $\cohdim_{\ell}(\ccat)$ of $\tcat$ is the smallest $n\geq 0$ or $\infty$ such that $\hh^i(\ccat,\shf) = 0$ for all $i>n$ and all torsion sheaves $\shf$ on $\tcat$.
    \glsadd{lcohomological-dimension}
    \item
    The \emph{cohomological dimension} of $\tcat$ is
    $\cohdim(\ccat) \defined \sup_{\ell}\lset \cohdim_{\ell}(\tcat)\rset$.
    \glsadd{cohomological-dimension}
  \end{enumerate}
\end{defn}

\begin{example}
    For any prime number $\ell$, the constant group scheme $\underline{\zz/\ell\zz}$ is a torsion sheaf.
\end{example}

\begin{theorem}
\label{coh-dim:main-theorem}
  Let $X$ be a $k$-scheme of finite type, where $k$ is a separably closed field.
  Then $\cohdim(\et{X})\leq 2\dim(X)$.
\end{theorem}

This theorem is supposed to be a generalization of the following statement from Galois cohomology:
\begin{theorem}[Tate]
    Let $K\sse L$ be a field extension.
    Then
    \[
    \cohdim_{\ell}(L)
    \leq
    \cohdim_{\ell}(K)
    +
    \trnsdeg_K(L),
    \]
    where we write $\cohdim_{\ell}(K)$ for the $\ell$-cohomologial dimension of $\et{\spec(K)}$, and similarly for $L$.
\end{theorem}

We start the proof of \cref{coh-dim:main-theorem} ---
it is a lengthy induction, which uses several lemma for the inductive step.
Throughout, a scheme $X$ will be as in \cref{coh-dim:main-theorem}, i.e. of finite type over a separably closed field.
For clearity, I want to phrase the lemma separately, so in the proofs of some of them we need to assume:
\begin{equation}
\tag{$\boxtimes$}
\label{coh-dim:indu-assumption}
  \text{
  The theorem holds for all
  schemes $X$ with $\dim(X) \leq n-1$.
  }
\end{equation}

\begin{lem}
  Let $\shf$ be a torsion sheaf on $X$ that has \emph{support in dimension $\leq n-1$}, i.e.
  \[
  \shf
  =
  \bigcup_{\lambda}i_{\lambda,\ast}\shg_{\lambda},
  \]
  where $i_{\lambda}\mc Z_{\lambda}\hookrightarrow X$ are closed subschemes of $X$ of dimension $n-1$, and $\shg_{\lambda}\in \shcat(\etindex{Z}{\lambda})$.
  Assume \eqref{coh-dim:indu-assumption}.
  Then
  $
  \hh^p(X,\shf)
  =
  0
  $
  for $p>2(n-1)$.
\end{lem}
\begin{proof}
  We have that $X$ is qcqs (\coms ?\come).
  So by \cref{cohomology:commutes-with-inverse-limits}, we get
  \[
  \hh^p(X,\shf)
  =
  \colim_{\lambda}\hh^p(X,i_{\lambda,\ast}\shg_{\lambda}).
  \]
  Since $i_{\lambda}$ is a closed immersion, we have that $i_{\lambda,\ast}$ is exact, and hence
  \[
  \hh^p(X,i_{\lambda,\ast}\shg_{\lambda}) = \hh^p(Z_{\lambda},G_{\lambda}).
  \]
  By \eqref{coh-dim:indu-assumption}, we have that $\hh^p(Z_{\lambda},G_{\lambda})$ vanishes for $p>2(n-1)$ (the $Z_{\lambda}$ are still of finite type over $k$), and so the result follows.
\end{proof}

\begin{lem}
  Assuming \eqref{coh-dim:indu-assumption}, it suffices to show \cref{coh-dim:main-theorem} for schemes that are irreducible and reduced.
\end{lem}
\begin{proof}
  \coms i don't understand it\come.
\end{proof}

\begin{lem}
\label{chlgy:replace-by-clsd-pnt}
  Let $X$ be a scheme that is of finite type over a field $L$ (which is not necessarily separably closed).
  Then there exists a separably closed field $L'$, a scheme $X'$ which is of finte type over $L'$ and a closed point $x'\in X'$ such that $\oo_{X,x}\sh \cong \oo_{X',x'}\sh$.
\end{lem}
\begin{proof}
  We can assume that $X$ is affine (because the colimit for $\oxx\sh$ is only over étale affine neighborhoods \coms ?\come).
  Let $Y\defined \overline{\lset x\rset}\sse X$, and set $d\defined \dim(Y)$.
  By Noether's normalization lemma, there exists a factorization
  \[
  \begin{tikzcd}
    X
    \ar{r}[above]{\pphi}
    &
    \affa_L^d
    \\
    Y
    \ar[hookrightarrow]{u}
    \ar{ur}
  \end{tikzcd},
  \]
  where $Y\to \affa_L^d$ is finite, and in particular dominant.
  So it maps $\eta_Y \defined x$ (the generic point of $Y$), to the generic point of $\affa_L^d$, which we denote by $\xi$.
  Let $L'$ be the fraction field of $\affa_L^d$ (i.e. the residue field of $\xi$).
  We then define $X'$ as the fiber product
  \[
  \begin{tikzcd}
    X'
    \ar[phantom]{rd}{\lrcorner}
    \ar{r}
    \ar{d}
    &
    X
    \ar{d}
    \\
    \spec(L')
    \ar{r}
    &
    \affa_L^d
  \end{tikzcd}
  \]
  where lower-horizontal map $\spec(L')\to \affa_L^d$ is induced by the inclusion of the generic point and the inclusion $\kappa(\xi)\sse L'$.
  Then $X'$ is a $L'$-scheme of finite type, and any point in the fiber over $x$ is a closed point of $X'$.
  We now claim that the stalks of $X'$ at $\overline{x'}$ and $X$ at $\overline{x}$ agree:
  We have that the map $L\to L'$ is weakly étale, since we can view it as the colimit over finite separable field extensions.
  (\coms I don't know how to finish this proof tbh\come)
\end{proof}

\begin{lem}
\label{chlgy:dimension-of-localizations}
	Let $R\to S$ be an étale morphism, $\idq\sse S$ a prime ideal and $\idp \defined \idq \cap R$.
	Then $\dim R_{\idp} = \dim S_{\idq}$.
\end{lem}

\begin{proof}
\leavevmode
\begin{itemize}
	\item
	Since the map $R_{\idp} \to S_{\idq}$ is flat and of finite presentation (since $R\to S$ is locally of finite presentation), it satisfies going down, and hence 
	\[\dim R_{\idp} \leq \dim S_{\idq}.\]
	\item 
	For the other bound, we note that étale morphisms are locally quasi-finite (since unramified morphisms are locally quasi-finite, c.f. \cite{stacks}*{\stackstag{03WS}}), and hence $R_{\idp}\to S_{\idq}$ is quasi-finite, so it has zero-dimensional discrete fibers, and hence 
	\[\dim R_{\idp} \geq \dim S_{\idq}.\]
\end{itemize}
\end{proof}

\begin{lem}
	Let $X$ be as in the theorem, and denote by $g\mc \eta\hookrightarrow X$ the inclusion of the generic point.
	Let $\shf \in \shetcat{\eta}$ be a sheaf.
	Then for every $j$, the sheaf $\rderived^j\directimage{g}\shf$ has support in dimensions $\leq n-j$.
\end{lem}

\begin{proof}
	The fiber product from \cref{chlgy:fibre-product-pushfw} reads as
	\[
	\begin{tikzcd}
		\displaystyle 
		\coprod \spec(L_i)
		\ar{r}
		\ar[hook]{d}
		&
		\eta
		\ar[hook]{d}
		\\
		\spec\left(\oxx\shnsl\right)
		\ar{r}
		&
		X
	\end{tikzcd}
\]
where we write $\idp_1,\ldots,\idp_r$ for the collection of minimal prime ideals of $\oxx\shnsl$ and $L_i \defined \fracfield(\oxx\sh/\idp_i)$.
We obtain this as follows --- to construct the fiber product, it suffices to consider an affine neighborhood $\spec(A)$ of $x$.
In that case, the situation reduces to a completely affine situation.
In fact, we can take $A = \oxx$, so the fiber product is given by $\spec(\oxx\sh \tensor_{\oxx} K)$, where we write $K$ for the function field of $X$.

	So for every geometric point $\overline{x} \to X$, we get:

	\[
		\left(\rderived^j \directimage{g}\shf\right)_{\overline{x}}
		\cong 
		\bigoplus_i 
		\hh^j\left(\spec(K_i),\restrict{\shf}{\spec(K_i)}\right)
	\]
	Let now $L'$, $x'$ and $X'$ be as in \cref{chlgy:replace-by-clsd-pnt}, and set $d\defined \dim \overline{\lset x\rset}$.
	Using \cref{chlgy:dimension-of-localizations}, we get:
	\begin{align*}
		\dim \oo_{X',x'}
		&=
		\dim \oo_{X',x'}\shnsl
		\\
		&=
		\dim \oxx\shnsl
		\\
		&=
		\dim \oxx
		=
		n-d.
	\end{align*}
%TODO: add this as a statement to the part about henselian rings
	We now need the following fact about dimension to continue:
	\begin{fact}
		Let $Y$ be an integral scheme that is of finite type over a separably closed field $k'$, then $\dim Y = \trnsdeg_{k'}\left(K(Y)\right)$.
	\end{fact}
%TODO: do we need k' separably closed for that?
	So in our case, $\trnsdeg_{L'}\left(\fracfield(\oo_{X',x'})\right) = n-d$ holds.

\end{proof}

