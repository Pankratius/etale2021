\section{Cohomology on the étale site}
We construct étale cohomology as right-derived functors of the global sections functor, and we'll take these constructions for granted.
Besides the ``usual'' references for this, the reader is invited to take a look at chapter 5 of \cite{jendrik-homalg} for detailed explanations, and at chapter 2 of \cite{hubible} for terminology on derived categories.
\subsection{Basic definitions}
\begin{prop}
  \label{chlgy:enough-inj}
  Let $X$ be a scheme.
  The category of sheaves on the étale site of $X$, $\shcat(\et{X})$, has enough injectives.
\end{prop}
\begin{proof}
  We basically mimic the proof that we already know for ``ordinary'' sheaves (i.e. on the Zariski site), which relies on $\abcat$ having enough injectives and stalks behaving nicely (both of which also holds for the étale site):
  Recall that for a geometric point $\overline{x}\mc \spec(K)\to X$ the category of sheaves on $\spec(K)$ is isomorphic to $\abcat$, and that the direct-inverse-image-adjunction simplifies to
  \[
  (-)_{\overline{x}}
  \mc
  \shcat(\et{X})
  \leftrightarrows
  \abcat
  \mcc
  \directimage{\overline{x}}
  \]
  where the assignement $\shf\mapsto \shf_{\overline{x}}$ of taking the stalk is actually exact.
  So $\directimage{\overline{x}}$ is actually exact, and we also have that $\left(\directimage{\overline{x}}I\right)_{\overline{x}} = I$ holds for any abelian group $I$.
  Let now $\shf$ be a sheaf on $\et{X}$.
  Since $\abcat$ has enough injectives, we can choose for every geometric point $\overline{x}$ an injection $\shf_{\overline{x}}\hookrightarrow I_{\overline{x}}$, where $I$ is an injective abelian group.
  Under the above adjunction, this gives a morphism $\shf\hookrightarrow \directimage{\overline{x}}I_{\overline{x}}$.
  Consider now the product of all of these, i.e. the map $\shf \to \prod_{\overline{x}}\directimage{\overline{x}}I_{\overline{x}}$.
  Then this is a map into an injective object (since arbitrary products of injective objects are again injective).
  The map itself is also injective, since this can be checked on stalks.
\end{proof}

\begin{defn}
  Let $X,Y$ be schemes.
  \begin{enumerate}
  \item
  We denote by $\rderived\Gamma(X,-)$ the right-derived functor
  \[
  \pderivedd(\shcat(\et{X}))
  \to
  \pderivedd(\abcat)
  \]
  of the global sections functor $\Gamma(X,-)\mc \shcat(\et{X})\to \abcat$.
  We denote the higher derived funtors
  $
  \rderived^i\Gamma
  \mc
  \shetcat{X}
  \to
  \abcat
  $
  by $\hh^n(\et{X},-)$.
  For a sheaf $\shf\in \shetcat{X}$, we call $\hh^n(\et{X},\shf)$ the \emph{$n$-th cohomology group} of $\shf$.
  \item
  Let $f\mc X\to Y$ be a morphism of schemes.
  The functor $\directimage{f}\mc \shetcat{X}\to \shetcat{Y}$ is left-exact, and we call it's right-derived functor \[\rderived\directimage{f}\mc \pderivedd(\shetcat{X})\to \pderivedd(\shetcat{Y})\] the \emph{derived direct image} functor; the induced functor $\rderived^n\directimage{f}\mc \shetcat{X}\to \shetcat{Y}$ is the ($n$-th) \emph{higher direct image}.
  \item
  For a fixed $\shf\in \shetcat{X}$, the functor
  \[
  \shetcat{X}\to \abcat,
  ~~
  \shg \mapsto \hom_{\shetcat{X}}(\shf,\shg)
  \]
  is left-exact.
  We write $\rderived\hom_{\shetcat{X}}(\shf,-)$ for the induced right-derived functor, and $\ext^n_{\et{X}}(\shf,-)\mc \shetcat{X}\to \abcat$ for the higher derived functors.
  More generally, the sheaf-hom functor $\ihom_{\et{X}}(\shf,-)\mc \shetcat{X}\to \shetcat{X}$ is left-exact, and we denote its right-derived functor by $\rderived\ihom_{\et{X}}(\shf,-)$, and $\iext_{\et{X}}^n(\shf,-)\mc \shetcat{X}\to \shetcat{X}$ for the higher derived functors.
  \end{enumerate}
\end{defn}
\begin{construction}
  Let $\begin{tikzcd}[column sep = small, cramped] Z \ar[hook]{r}[above]{i}&X \ar[hookleftarrow]{r}[above]{j}& U\end{tikzcd}$ be as in \cref{subsection:six-functors}, i.e. $i$ is a closed immersion, $j$ is an open immersion and $Z = X\setminus U$ holds on the level of topological spaces.
  We showed that the functor $i^{!}\mc \shetcat{X}\to\shetcat{Z}$ is left-exact, and so the composition
  \[
  \Gamma(Z,i^{!}(-))
  \mc
  \shetcat{X}
  \to \abcat
  \]
  is too.
  We can regar it as \emph{functor of global sections with support in $Z$}, i.e. for every sheaf $\shf \in \shetcat{X}$, we have
  \[
  \Gamma(Z,i^{!}\shf)
  =
  \Gamma(X,\directimage{i}i^{!}\shf)
  =
  \ker
  \left(
  \shf(X)\xrightarrow{\restrict{(-)}{U}}
  \shf(U)
  \right),
  \]
  and $\directimage{i}i^{!}\shf$ is the biggest subsheaf of $X$ which is zero on $X\setminus Z$.
  We write $\hh_Z^n(\et{X},-)$ for the higher derived functors of $\rderived\Gamma(Z,i^{!}(-))$.
  For a sheaf $\shf\in \shetcat{X}$, we say that $\hh^n_Z(\et{X},\shf)$ is the $n$-th \emph{cohomology group of $\shf$ with support in $Z$}.
\end{construction}


\begin{rem}
  One might wonder if there are also ``higher inverse images'' functor for a map $f\mc X\to Y$.
  But in fact, we don't need to derive $\inverseimage{f}$, since in our setup $\inverseimage{f}\mc \shetcat{Y}\to \shetcat{X}$ is exact.
  Note that this is different from the case of quasi-coherent modules on Zariski sites, because in that case, we need some tensoring for $\inverseimage{f}$ to even make sense, which induces flatness problems.
  But if we only consider sheaves with no additional module structure, we don't have that problem -- yay!
  In particular, for any morphism $f\mc X\to Y$, the pullback along $f$ induces a functor
  \[
  \inverseimage{f}
  \mc
  \pderivedd(\shetcat{Y})
  \to
  \pderivedd(\shetcat{X}),
  \]
  \coms when does $f$ preserve injective objects?\come
\end{rem}

\begin{lem}
  Let $\shf\in \shetcat{X}$ be a sheaf and $f\mc U\to X$ an étale morphism.
  Then the diagram
  \[
  \begin{tikzcd}
    \pderivedd(\shetcat{U})
    \ar{rr}[above]{\rderived\Gamma(\et{U},-)}
    \ar{rd}[below left]{\inverseimage{f}}
    &
    &
    \pderivedd(\abcat)
    \\
    &
    \pderivedd(\shetcat{X})
    \ar{ur}[below right]{\rderived\Gamma(\et{X},-)}
    &
  \end{tikzcd}
  \]
  commutes.
\end{lem}
\begin{proof}
  This follows from generalities on how to compose functors between derived categories, see below for more.
\end{proof}
\begin{defn}
  In light of this propostion, we only write $\hh^n(\shetcat{U},\shf)$ for the equal cohomology groups of a sheaf $\shf\in \shetcat{X}$.
\end{defn}

\begin{prop}[\cite{stacks}*{\stackstag{015L}}]
\label{cohomology:grothendieck-spec-seq}
\leavevmode
  \begin{enumerate}
    \item
    \label{cohomology:grothendieck-spec-seq:prelim}
    Let $F\mc \acat\to \bcat$ and $G\mc \bcat \to \ccat$ be left-exact functors between abelian categories, and assume that both $\acat,\bcat$ have enough injective objects.
    Then the following are equivalent:
    \begin{enumerate}
      \item
      \label{cohomology:grothendieck-spec-seq:prelima}
      The complex $F(I)$ is right-acyclic for $G$ for every injective object $I$ of $\acat$; and
      \item
      \label{cohomology:grothendieck-spec-seq:prelimb}
      The canonical map $\rderived(G\circ F)\to \rderived G\circ \rderived F$ is an isomorphism of functors $\pderivedd(\acat)\to \pderivedd(\ccat)$.
    \end{enumerate}
    \item
    \label{cohomology:grothendieck-spec-seq:statement}
    Assume that we're in the setup of (i) and (a)/(b) holds.
    Let $X\in \pderivedd(\acat)$ be an object.
    Then there exists a spectral sequence $(E_r,d_r)_{r\geq 0}$ consisting of bigraded objects of $\ccat$ with $d_r$ of bidegree $(r,-r+1)$ and with
    \[
    E_2^{p,q}
    =
    \rderived^pG\left(\hh^q(\rderived F(X))\right).
    \]
    Moreover, this spectral sequence is bounded, converges to $\hh^{\bullet}(\rderived(G\circ F)(X))$ and induces a finite filtration on each $\hh^n(\rderived(G\circ F)(X))$.
    We call this spectral sequence the \emph{Grothendieck spectral sequence}.
  \end{enumerate}
\end{prop}

\begin{defn}
  A sheaf $\shf \in \shetcat{X}$ is called \emph{flasque}\footnote{or ``flabby'', but we're already deep in the french words game anyways.} if $\hh^n(\shetcat{U},\shf) = 0$ holds for all étale $U\to X$.
\end{defn}

\begin{lem}
\label{cohomology:flasque-are-acyclic}
  Let $\shf\in \shetcat{X}$ be a flasque sheaf, and $f\mc X\to Y$ a morphism of schemes.
  Then $\shf$ is \emph{$f$-acyclic}, i.e. $\rderived^n\directimage{f}\shf = 0$ holds for $n\geq 1$.
\end{lem}
\begin{proof}
  Recall that the functor $\iota\mc \shetcat{X}\to \preshcat(\et{X})$ is left-exact;
  denote by $\rderived\iota$ its derived functor, and by $\underline{\hh}^n(\shf)$ the $n$-th higher derived functor of $\iota$.
  \begin{claim}
    For every sheaf $\shf \in \shetcat{x}$, the presheaf $\underline{\hh}^n(\shf)$ is given by the assignement $U \mapsto \hh^n(U,\shf)$ for $U$ étale over $X$.
  \end{claim}
  Let now $f\mc X\to Y$ be a morphism of schemes, and denote by
  \[
  f_p
  \mc
  \preshcat(\et{X})
  \to
  \preshcat(\et{Y})
  \]
  the pushforward on presheaves.
  \begin{claim}
	\label{cohomology:flasque-are-acyclic-2c}
    For every sheaf $\shf\in \shetcat{X}$, it holds that $\rderived^n\directimage{f}\shf = \left(f_p \underline{\hh}^n(\shf)\right)\sh$.
  \end{claim}
  Equipped with these two facts, the proof of the original lemma doesn't take too long:
  That $\shf$ is flasque means $\underline{\hh}^n(\shf)=0$ for all $n\geq 1$, and hence by the second claim $\rderived^n\directimage{f}\shf = 0$ for all $n\geq 1$.
\end{proof}

\begin{prop}
\label{cohomology:commutes-with-inverse-limits}
  Let $X = \lim X_i$ be a limit of a directed system of qcqs schemes along affine transition maps $f_{i',i}$, such that all of the $X_i$ are qcqs too.
  Let $\shf_i\in \shcat(\etindex{X}{i})$ together with maps
  \[
  \pphi_{i',i}
  \mc
  \inverseimage{f_{i',i}}(\shf_i) \to \shf_{i'}
  \]
  be a compatible system of sheaves on the $X_i$.
  Let $f_i \mc X\to X_i$ be the canonical maps, and $\shf \defined \colim \inverseimage{f_i}\shf_i$.
  Then:
  \[
  \colim \hh^n(\etindex{X}{i},\shf_i)
  =
  \hh^n(\et{X},\shf).
  \]
\end{prop}
\begin{proof}
  The proof of this uses \v{C}ech-cohomology, so we defer it until we make it there.
\end{proof}

\begin{cor}
	\label{chlgy:fibre-product-pushfw}
	Let $f\mc X\to Y$ be a quasi-compact morphism of schemes, $y\in $ a point and $\shf\in \shetcat{X}$.
  Consider again the fiber-product from \cref{etsheaves:more-pullback-pushforward-props}, \ref{etsheaves:more-pullback-pushforward-props:ii}:
  \[
  \begin{tikzcd}
    \wtilde{X}
    \ar{r}[above]{\wtilde{f}}
    \ar{d}
    \ar[phantom]{rd}{\lrcorner}
    &
    X
    \ar{d}[right]{f}
    \\
    \spec(\oo_{Y,\overline{y}}\sh)
    \ar{r}
    &
    Y
  \end{tikzcd}
  \]
  Then the isomorphism $\left(\directimage{f}\shf\right)_{\overline{x}} \smalliso \Gamma(\wtilde{X},\inverseimage{\wtilde{f}}\shf)$ extends to an isomorphism
  \[
  \left(\rderived^n \directimage{f}\shf\right)_{\overline{x}}
  \isomorphism
  \hh^n(\et{\wtilde{X}},\inverseimage{\wtilde{f}}\shf).
  \]
\end{cor}

\begin{proof}
  To show that the morphism exists, we're going to use the universal property of cohomological functors:
  The cohomological functors $\left(\rderived^n\directimage{f}(-)\right)_{\overline{x}}$ are universal (because $\rderived\directimage{f}$ is and taking stalks is exact);
  the functors $\hh^n(\et{\wtilde{X}},\inverseimage{\wtilde{f}}(-))$ is also a cohomological functor (again because $\wtilde{f}$ is exact).
  So the isomorphism $\left(\directimage{f}\shf\right)_{\overline{x}} \smalliso \Gamma(\wtilde{X},\inverseimage{\wtilde{f}}\shf)$ extends to a morphism between the higher derived functors.
  \par
  We will check on stalks that it is an isomorphism:
  We've seen in the proof of \cref{cohomology:flasque-are-acyclic} that we can write $\rderived^n\directimage{f}$ as the sheafification of the presheaf \[U\mapsto \hh^n(X\times_Y U,\shf)\] on $\et{Y}$.
  In particular, we can use the presheaf description of $\rderived^i\directimage{f}\shf$ to calculate it's stalks:
  \[
  \left(
  \rderived^i\directimage{f}\shf
  \right)_{\overline{y}}
  \cong
  \colim_{(U,\overline{u})\in \ncataff_{\overline{y}}}
  \hh^n(X\times_Y U,\shf),
  \]
  where $\ncataff_{\overline{y}}$ is the category of all affine étale neighborhoods of the geometric point.
  So we're in the situation of the proposition, which tells us that the right hand side equals $\hh^n(\et{\wtilde{X}},\inverseimage{\wtilde{g}}\shf)$
\end{proof}
