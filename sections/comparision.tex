\subsection{Comparision results}

\subsubsection*{Flat vs. étale}

\begin{prop}
	Let $\ggroup$ be a smooth, quasi-projective, commutative group scheme over a scheme $X$.
	Then the the natural map $\xi \mc \fppf{X} \to \et{X}$ induces natural isomorphisms
	\[
		\hh^i(\et{X},\ggroup)
		\isomorphism
		\hh^i(\fppf{X},\ggroup)
	\]
	for all $i \geq 0$.

\end{prop}

\begin{rem}
	Over a field of characteristic zero, every locally algebraic group scheme\footnote{A group scheme $\ggroup$ over a field $k$ is \emph{(locally) algebraic} if it is (locally) of finite type over $\spec(k)$.} is smooth.
	If if $k$ is a perfect field with $\rchar(k) = p > 0$ and $G$ is reduced, then $G$ is smooth.
	\coms I don't know what happens if $k$ is not perfect \come 
	For any field $k$, and $\underline{G}$ a constant group scheme with $G$ a finite group, $\underline{G}$ is smooth over $k$ (since $\underline{G} \cong \spec\left( \prod_{g \in G} k\right)$, and this is even étale over $k$).
\end{rem}
\subsubsection*{Zariski vs. étale}
\begin{numtext}
	Recall that if $\shf$ is a sheaf on $\zar{X}$ for a scheme $X$, then it gives a sheaf on $\et{X}$, which we denote by $W(\shf)$ and which is given by 
	\[
		W(\shf)
		\mc
		\left(U \xrightarrow{f} X\right)
		\longmapsto
		\Gamma(U,\inverseimage{f}\shf)
	.\] 
	In the following, we will be interested in the case where $\shf$ is a sheaf of $\ox$-modules on $\zar{X}$.
	Recall also that we have a natural morphism of sites $\zeta\mc \et{X} \to \zar{X}$ (since open immersions are étale).
	These two constructions are compatible in the sense that $\directimage{\zeta} W(\shf) = \shf$ holds for $\shf \in \shcat(\zar{X})$.
\end{numtext}

\begin{prop}
	Let $X$ be a scheme and $\shf$ be a quasi-coherent sheaf of $\ox$-modules on $\zar{X}$.
	Then the natural map $\zeta\mc \et{X} \to \zar{X}$ induces natural isomorphisms 
	\[
		\hh^i(\zar{X},\shf)
		\isomorphism
		\hh^i(\et{X},W(\shf))
	\]
	for all $i \geq 0$.
\end{prop}

\begin{proof}
	Taking global sections commutes with $\directimage{\zeta}$, i.e. we have the following commutative diagram of functors:
	\[
		\begin{tikzcd}
		\shetcat{X}
		\ar{r}[above]{\directimage{\zeta}}
		\ar{rd}[below left]{\Gamma(X,-)}
		&
		\shcat{\zar{X}}
		\ar{d}[right]{\Gamma(X,-)}
		\\
		&
		\abcat
		\end{tikzcd}
	\]
	So we get a spectral sequence 
	\[
		E_2^{i,j}
		=
		\hh^i\left(\zar{X},\rderived^j\directimage{\zeta}W(\shf)\right)
		\Rightarrow
		\hh^{i+j}\left(\et{X},W(\shf)\right)
	.\] 
	So it suffices to show that for $j \geq 1$ it holds that
	\begin{equation}
		\label{comparision:zar-vanish-claim}
		\tag{$\ast$}
		\rderived^j \directimage{f} W(\shf)
		=
		0.
	\end{equation}
	Now similar to \cref{cohomology:flasque-are-acyclic-2c} from the proof of \cref{cohomology:flasque-are-acyclic}, we have that $\rderived^j \directimage{\zeta} W(\shf)$ is the sheafification of the Zariski-presheaf
	\[
		U
		\longmapsto
		\hh^j(\et{U},W(\shf))
	.\] 
	So it suffices to show $\hh^j(\et{U},W(\shf)) = 0$ for all $U \sse X$ open affine and $j \geq 1$, or that $W(\shf)$ is a flasque sheaf on the affine-étale-finite-type site over $U$, for any $U \sse X$ open affine.
	We denote the latter site by $\ccat$.
	\footnote{
		We can make the restriction from the étale site over $U$ to the affine-étale-finite-type site over $U$, because every any cover on $\et{U}$ can be refined to a cover that is contained in the latter site.
	Furthermore, I think that the notation $W(\shf)$ is a bit sloppy if we want to get a sheaf on $\et{U}$ or $\ccat$, because strictly speaking we're taking the pullback along the inclusion $U \hookrightarrow X$.
}
	By \cref{cechcoh:flasque-on-cech},  it suffices to show that $\chhlgy^j(V'/V,W(\shf)) = 0$ holds for all étale morphisms of finite type $V'\to V$ over $U$ and $j \geq 1$.
	Assume $V' = \spec(B)$ and $V = \spec(A)$.
	Since everything is affine, we can write $\shf = \wtilde{M}$ for an $A$-module $M$.
	\footnote{
	Where this $\shf$ means the further pullback along $V' \to U$. Note that this pullback of $\shf$ is again a quasi-coherent $V'$-module.
	}
	We need to show that the \v{C}ech complex 
	\begin{equation}
	\tag{$\boxtimes$}
	\label{comparision:cech}
		\begin{tikzcd}[column sep = small, cramped]
		0
		\ar{r}
		&
		M \tensor_A B
		\ar{r}
		&
		M \tensor_A B \tensor_A B 
		\ar{r}
		&
		M \tensor_A (B^{\tensor_A^3})
		\ar{r}
		&
		\ldots		
		\end{tikzcd}
	\end{equation}
	is exact.
	For that, we use the following general trick:
	\begin{claim}
		Showing that \eqref{comparision:cech} is exact can be done after base-changing along any faithfully flat map $A \to A'$.	
	\end{claim}
	\begin{claimproof}
		Set $B' \defined A' \tensor_A B$ and $M' \defined A' \tensor_A M$.
		We obtain the \v{C}ech complex for these new rings/modules after tensoring \eqref{comparision:cech} over $A$ with $A'$.
		Now this just faithfully flat descent along $A \to A'$.
	\end{claimproof}
	Consider now the special instance of this claim in which $A' = B$.
	Then the resulting map $A' \to B'$ admitsa section (given by $b\tensor c \mapsto bc$), and which we can use to construct a contracting homotopy of the \v{C}ech complex \eqref{comparision:cech} for $A'$ and $B'$.
	By the above claim, this suffices to conclude that \eqref{comparision:cech} vanishes for $A$ and $B$ too.
\end{proof}

\begin{prop}
	\label{comp:et-zar-shnsl}
	\leavevmode
	\begin{enumerate}
		\item
		\label{comp:et-zar-shnsl:vanish}
			Let $S$ be a scheme such that all local rings of $S$ are strictly henselian.
			Then for any sheaf $\shf \in \shetcat{S}$, we have $\hh^i(\et{S},\shf) = \hh^i(\zar{S},\shf)$ for $i\geq 0$.
		\item
		\label{comp:et-zar-shnsl:agree}
			Let $S$ be an affine scheme such that:
			\begin{enumerate}
				\item
					All points of $S$ are closed; and 
				\item
					All residue fields of $S$ are separably closed.
			\end{enumerate}
			Then for any sheaf $\shf \in \shetcat{X}$ and $i\geq 1$, we have 
			\[
				\hh^i(\et{S},\shf) = 0.
			\]
	\end{enumerate}
\end{prop}

\begin{proof}
	\leavevmode
	\begin{enumerate}
		\item
			Recall that for a strictly henselian local ring $R$ and $Y\defined \spec(R)$ with unique closed point $\overline{y}$, the global section functor is given by the stalk functor, i.e. $\Gamma(Y,-)$ is given as
			\[
				\Gamma(Y,-)
				\mc
				\shetcat{Y}
				\to 
				\abcat
				,
				~
				\shf 
				\mapsto 
				\shf_{\overline{y}}
			.\] 
			Since taking stalks is exact, we have $\hh^i(\et{Y},\shf) = 0$ for all $i\geq 0 $ and $\shf\in \shetcat{Y}$.
			\par
			To get the claimed result for $S$, we show (similar to the proposition above), that for the natural map $\zeta \mc \et{X} \to \zar{X}$ all higher direct images vanish, i.e. $\rderived^i \directimage{\zeta} \shf = 0$ for $i\geq 1$.
			Again, this would finish the proof by the Leray spectral sequence.
			To show that the higher direct images vanishes, we recall that the Zariski sheaf $\rderived^p \directimage{\zeta} \shf$ is the Zariski-sheafification of the Zariski-presheaf $V(\shf)\mc U \mapsto \hh^p(\et{U},\shf)$ (\coms did we already show this? i'm not sure rn\coms).
			%TODO: For F étale sheaf, higher direct zarsiki image is given by Zar-sheafification of assigning et. cohomology.
	So for $x\in S$, we have 
		\begin{align*}
			\left(\rderived^q \directimage{\zeta}\shf\right)_{x}
			=
			V(\shf)_x
			&
			=
			\colim_{\substack{x\in U \sse X\\ \text{op. im.}}}
			\hh^q(U,\shf)
			\\
			&
			=
			\hh^q(\et{\spec(\oxx)},\shg)
			\\
			&
			=
			0,
		\end{align*}
		where we used the initial observation and the assumption that all $\oxx$ are strictly henselian to get the vanishing in the end, and $\shg$ is the pullback of $\shf$ along the map $\spec(\oxx) \to X$.
		\item
			In this case, all local rings are henselian (this holds true for any zero-dimensional local ring), and since we assume that the residue fields are separably closed, they are even strictly henselian.
			So we can apply \ref{comp:all-loc-shnsl:vanish}, and compute étale cohomology via Zariski cohomology.
			I don't know how to finish the proof in this case on my own, but the Stacks project says that in this case $S = \spec(R)$ is a profinite topological space, and that then the vanishing of higher cohomology groups on $\zar{S}$ is a general fact (see \cite{stacks}*{\stackstag{09AY}} for more).
	\end{enumerate}
\end{proof}
\subsubsection*{Étale vs. complex analytic}

\begin{theorem}
	Let $X$ be a smooth scheme over the complex numbers.
	Then for all $i \geq 1$ and every finite abelian group, the étale cohomology of $X$ with coefficients in the constant sheaf $\underline{M}$ and the singular cohomology with coefficients in $M$ of the $\cc$-valued points of $X$ (endowed with the complex analytic topology) agree: 
	\[
		\hh^i_{\mathrm{sing.}}
		(X(\cc),M)
		\cong
		\hh^i(\et{X},\underline{M}).
	\] 
\end{theorem}

\begin{rem}
	The same does no longer work when $M$ is not a finite abelian group.
	For example, for $X$ a smooth proper curve of genus $g$, we have $\hh^1(X(\cc),\zz) \cong \zz^{2g}$.
	\footnote{$X(\cc)$ is the ``torus with $g$ holes''}
	However, $\hh^1(\et{X},\underline{\zz}) \cong \hom(\pi_1^{\mathrm{\acute{e}t.}}(X),\zz)$, where $\pi_1^{\mathrm{\acute{e}t.}}(X)$ is the étale (!) fundamental group of $X$.
	This is a profinite group, and hence  $\hom(\pi_1^{\mathrm{\acute{e}t.}}(X),\zz) = 0$.
\end{rem}
