\section{Local rings in the étale topology}
\subsection{Towards stalks of sheaves on the étale site}
\begin{numtext}
\label{locring:etale-nhod-initials}
  The goal of this section is to describe how the ``local rings on the étale site'' should look like.
  We will do this by immitating the construction of the local rings on the (small) Zariski site:
  recall the stalk of the structure sheaf at a point $x$ is defined as
  \[
  \oxx
  =
  \varinjlim_{x\in U \sse X} \Gamma(U,\ox).
  \]
  In other words, we're taking the colimit over all diagrams of the form
  \[
  \begin{tikzcd}
    \spec(\kappa(x))
    \ar{r}
    \ar{dr}
    &
    U
    \ar{d}[right]{\substack{\text{open}\\\text{immersion}}}
    \\
    &
    X
  \end{tikzcd}
  \]
  One natural way to carry this over to the étale setting would be to consider diagrams of the form:
  \[
  \begin{tikzcd}
    \spec(\kappa(x))
    \ar{r}
    \ar{rd}
    &
    U
    \ar{d}[right]{\text{étale}}
    \\
    &
    X
  \end{tikzcd}
  \]
  But as it turns out, this construction ``does not yield enough'' neighborhoods.
  For lack of a better word, I'll call them \emph{``na\"{i}ve-étale''} neighborhoods (and they come with the associated category of na\"{i}ve-étale neighborhoods'' of $x$, $\nvcat_x$), but this is just a word I've come up with.\par
  Instead what we have to get the ``right'' \emph{étale neighborhoods} of a point $x\in X$ is to consider all diagrams of the form
  \[
  \begin{tikzcd}
    \spec(K)
    \ar{r}
    \ar{rd}
    &
    U
    \ar{d}[right]{\text{étale}}
    \\
    &
    X
  \end{tikzcd}
  \]
  where $K$ is a separably closed field, and the point of $\spec(K)$ gets mapped to $x$.\footnote{I write $K$ instead of the usual $k$ for an arbitrary separated field for now, to distinguish it from any $k$'s we might want to use for the residue field of a local ring.}
  If we spell out what being a (na\"{i}ve)-étale neighborhood means on residue fields of local rings, we can see the difference of the definitions quite well:
  \[
  \begin{tikzcd}
    \kappa(x)
    \ar[hookleftarrow]{r}
    &
    \kappa(u)
    \\
    &
    \kappa(x)
    \ar[hook]{u}
    \ar[equal]{ul}
  \end{tikzcd}
  ~~
  \text{vs.}
  ~~
  \begin{tikzcd}
    K
    \ar[hookleftarrow]{r}
    \ar[hookleftarrow]{dr}
    &
    \kappa(u)
    \\
    &
    \kappa(x)
    \ar[hook]{u}
  \end{tikzcd}
  \]
  So in the case of an na\"{i}ve-étale neighborhood of $x$, we want to consider points in $U$ that have the same residue field as $x$, but in the (actual) étale case we're considering points with embeddings into a separably closed field.
  Since considering all possible maps into separably closed fields at once might lead to set-theoretic problems at some point (\come does it?\come), we want (for now, c.f. ...)  that different separably closed fields lead to different categories of étale neighborhoods.
  Summarizing, we make the following definitions:
\end{numtext}
\begin{defn}
  Let $X$ be a scheme and $x\in X$ a point.
  \begin{enumerate}
    \item
    A \emph{geometric point of $X$, associated to $x$} is a morphism of schemes
    \[
    \overline{x}\mc \spec(K)\to X,
    \]
    for $K$ a separably closed field, that has $x$ as topological image\footnote{Note the ambiguity: a point $x\in X$ can have many different geometric points associated to it. So being ``a geometric point'' is really not a property of the point $x$ itself!}.
    \item
    Let $\overline{x}\mc \spec(K)\to X$ be a geometric point of $X$.
    An \emph{étale neighborhood of the morphism $\overline{x}$} is an étale $X$-scheme $U$ together with a geometric point of $U$, say $\overline{u}\mc \spec(K)\to U$, such that the diagram
    \[
    \begin{tikzcd}
      \spec(K)
      \ar{r}[above]{\overline{u}}
      \ar{rd}[below left]{\overline{x}}
      &
      U
      \ar{d}[right]{\text{étale}}
      \\
      &
      X
    \end{tikzcd}
    \]
    A morphism of étale neighborhoods of $\overline{x}$, say $(U,\overline{u})\to (V,\overline{v})$, is given by a morphism of $X$-schemes $f\mc U\to V$ such that the diagram
    \[
    \begin{tikzcd}[row sep = small]
      &
      V
      \ar{dr}
      &
      \\
      \spec(K)
      \ar{rr}[above, near start]{\overline{x}}
      \ar{ur}[above left]{\overline{v}}
      \ar{dr}[below left]{\overline{u}}
      &
      &
      X
      \\
      &
      U
      \ar[crossing over]{uu}[right, near start]{f}
      \ar{ur}
      &
    \end{tikzcd}
    \]
    commutes.
    This gives the category $\ncat_{\overline{x}}$ of étale neighborhoods of the morphism $\overline{x}$.
    \glsadd{etale-nhood}
  \end{enumerate}
\end{defn}

\begin{lem}
\label{locrings:etale-nhoods-are-cofiltered}
  \leavevmode
  \begin{enumerate}
    \item
    Let $x\in X$ be a point.
    Then the category $\nvcat_x$ of na\"{i}ve-étale neighborhoods of $x$ is cofiltered.
    \item
    Similarly, let $\overline{x}\mc \spec(K)\to X$ be a geometric point.
    Then the category $\ncat_{\overline{x}}$ of étale neighborhoods of the morphism $\overline{x}$ is cofiltered.
  \end{enumerate}
  In both cases, the subcategories of affine neighborhoods are cofiltered too.
\end{lem}
\begin{proof}
  We only show (ii), the rest should be similar.
  Using the enumeration as in \cite{stacks}*{\stackstag{04AY}}, we have:
  \begin{enumerate}[label = (\arabic*)]
    \item
    The category $\ncat_{\overline{x}}$ is non-empty, as $(X,\overline{x})$ is an étale neighborhood of $\overline{x}$.
    \item
    For all étale neighborhoods $(U,\overline{u})$ and $(V,\overline{v})$, we can use the fiber product $W\defined U\times_X V$ and the morphism $\overline{w}\mc(\overline{u},\overline{v})\mc \spec(K)\to W$ to define the common object with to both $(U,\overline{u})$ and $(V,\overline{v})$.
    \item
    Let $\zeta_1,\zeta_2\mc (U,\overline{u})\rightrightarrows (V,\overline{v})$ be two parallel morphisms in $\ncat_{\overline{x}}$.
    Then their equalizer $\equalizer(\zeta_1,\zeta_2)$ is an open subscheme of $U$ (\cref{defn:etale-equalizer}), and since the compositions
    \[
    \begin{tikzcd}[column sep = small]
    \spec(K)
    \ar{r}[above]{\overline{u}}
    &
    U
    \ar[shift left = .75]{r}[above]{\zeta_1}
    \ar[shift right = .75]{r}[below]{\zeta_2}
    &
    V
    \end{tikzcd}
    \]
    agree (they both equal $\overline{v}$), we get a factorization of $\overline{u}$ over $\equalizer(\zeta_1,\zeta_2)$, which gives the desired equalizing étale neighborhood.
  \end{enumerate}
\end{proof}
\begin{rem}[\cite{frankenotes}*{1.6.6}]
\label{locrings:canonical-etale-nhood-cat}
  So far, we distinguished between different separable closed fields and associated geometric points $\overline{x}\mc \spec(K)\to X$.
  For a fixed $x\in X$, we can however construct a ``canonical'' category of étale neighborhoods, in the following sense:
  Fix a separable closure of $\kappa(x)$, say $k\sepclos$.
  Then this defines a geomtric point $\overline{x}\sepclos\mc \spec(k\sepclos)\to X$.
  Now for any other geometric point $\overline{x}$ associated to $x$ (i.e. the topological image of $\overline{x}$ is given by $x$), the categories $\ncat_{\overline{x}}$ and $\ncat_{\overline{x}\sepclos}$ are equivalent:
  Using the interpretation of étale neighborhoods from \cref{locring:etale-nhod-initials}, we see that for any étale neighborhood $\spec(K)\to U$ of a geometric point $\overline{x}\mc \spec(K)\to X$, the image of $\kappa(u)\to K$ is contained in $k\sepclos$.
  So there's a unique $\overline{u}'\mc \spec(k\sepclos)\to U$ such that the diagram
  \[
  \begin{tikzcd}
    &
    U
    \\
    \spec(\kappa(x))
    \ar{r}
    \ar{ur}[above left]{\overline{u}}
    &
    \spec(k\sepclos)
    \ar{u}[right]{\overline{u}'}
  \end{tikzcd}
  \]
  commutes.
  \par
  So if we do not specify a map $\overline{x}\mc \spec(K)\to X$ when talking about a geometric point of $X$, then the $\overline{x}$ in $\ncat_{\overline{x}}$ will mean the ``canonical'' geometric point associated to $x$, i.e. precisely this map $\spec(\kappa(x)\sepclos)\to X$ from above.
\end{rem}

\subsection{Henselian rings}
\begin{defn}
  Let $A$ be a local ring with maximal ideal $\idm$ and residue field $k \defined A/\idm$.
  \begin{enumerate}
    \item
    We say that $A$ is \emph{henselian} if the conclusion of Hensel's lemma holds, i.e. for every monic polynomial $f\in A[t]$ with reduction $\overline{f} \in k[t]$ that has a factorization $\overline{f} = \overline{g}\cdot \overline{h}$ with $\overline{g},\overline{h}\in k[t]$ monic and coprime, there are monic $g,h\in A[t]$ with reductions $\overline{g},\overline{h}$ respectively, such that $f = gh \in A[t]$.
    \item
    We say that $A$ is \emph{strictly henselian} if it is henselian and $k$ is algebraically closed.
  \end{enumerate}
\end{defn}
\begin{rem}
\label{locring:hensel-addprops}
  The lifts $g,h$ from the above factorization have automatically the following additional properties:
  \begin{enumerate}
    \item
    \label{locring:hensel-addprops:coprime}
    They are \emph{strictly coprime}, i.e. $(g,h) = A[T]$:
    Their images in $\overline{g},\overline{h}$ in $k[t]$ are coprime, and since they are monic (it actually suffices that one of them is monic), we have that $C \defined A[t]/(f,g)$ is a finitely generated $A$-module.
    Since $(\overline{g},\overline{h}) = k[t]$ holds, it follows that $(g,h) + \idm A[t] = A[t]$ and $\idm M = \idm$, which implies $M = 0$, by Nakayama.
    \item
    They are unique:
    %TODO
  \end{enumerate}
\end{rem}
\begin{prop}[\cite{milne}*{Theorem I.4.3}]
\label{locring:hensel-char}
  Let $A$ be a local ring and $x\in X=\spec(A)$ a closed point, corresponding to a maximal ideal $\idm\sse A$, and write $k\defined A/\idm A$ for the residue field.
  Then the following are equivalent:
  \begin{enumerate}
    \item
    The ring $A$ is henselian.
    \item
    \label{locring:hensel-char:finite-decomp}
    If $f\mc Y\to X$ is a finite morphism of schemes, then $Y = \coprod_{i=1}^n \spec(B_i)$ holds, where each of the $B_i$ is a local finite $A$-algebra.
    \item
    If $f'\mc Y'\to X$ is a morphism of schemes that is quasi-finite and separated, then $Y' = Y_0 \sqcup \coprod_{i=1}^n \spec(B_i)$ holds, where $x$ is not contained in the image of $Y_0$, and each of the $B_i$ is a local finite $A$-algebra.
    \item
    \label{locring:hensel-char:etale-lifting}
    If $U\to X$ is étale, then any lift $\spec(k)\to U$ of $\spec(k)\to X$ extends to a unique section $X\to U$ of $U\to X$:
    \[
    \begin{tikzcd}
      &
      U
      \ar{d}[right]{f}
      \\
      \spec(k)
      \ar[dashed]{ur}
      \ar{r}
      &
      X
    \end{tikzcd}
    ~
    \leadsto
    ~
    \begin{tikzcd}
      &
      U
      \ar{d}[right]{f}
      \\
      \spec(k)
      \ar[dashed]{ur}
      \ar{r}
      &
      X
      \ar[bend right = 45, dashed]{u}
    \end{tikzcd}
    \]
    \item
    If $f_1,\ldots,f_n \in A[t_1,\ldots,t_n]$ are polynomials and $x_0\in k^n$ a common zero of the $\overline{f_j}$, and $\det(\partial_i f_j)(x_0)\neq 0$ holds, then there is a unique $x\in A^n$ which is a common zero of the $f_j$ and which has image $x_0$ in $k^n$.
    \item
    The same condition as in the definition of henselian rings holds, but we only require one of the polynomials to be monic, and only its lift will be monic.
  \end{enumerate}
\end{prop}
\begin{proof}
  For (i) $\Rightarrow$ (ii), we first note that $Y$ is automatically affine (\cref{app:finite-over-affine-is-affine}), say $Y = \spec(B)$ for a finite $B$-algebra $A$.
  We also have by the going-up theorem that every maximal ideal of $B$ lies over $\idm$, and hence $B$ is local if and only if $B/\idm B$ is local.
  We now show the implication in two steps:
  \begin{itemize}
    \item
    Assume that $B = A[t]/(f)$, for a monic polynomial $f\in A[t]$.
    Let $\overline{f} = \prod_{i=1}^n \overline{p_i}^{e_i}$ be the factorization of $\overline{f}$ into irreducibles in $k[t]$.
    Since $A$ is henselian, we get a factorization $f = \prod_{i=1}^n p_i^{e_i}$ in $A[t]$, where the $p_i^{e_i}$ are strictly coprime (\cref{locring:hensel-addprops},\ref{locring:hensel-addprops:coprime}).
    If we set $B_i\defined A[t]/(p_i^{e_i})$, then the Chinese remainder theorem gives a decomposition
    \[
    A[t]/(f)
    \cong
    \prod_{i=1}^n B_i.
    \]
    Now each of the $B_i$ is local, since $B_i/\idm B_i \cong k[t]/(\overline{p_i}^{e_i})$ is an artinian local $k$-algebra (since the $p_i$'s are irreducible).
    This gives the desired decomposition of $\spec(B)$.
    \item
    For the general case, we first observe that since $B$ is finite over $A$, we can decompose $\spec(B)$ into finitely many affine connected components.
    In particular, we may assume that $\spec(B)$ is connected.
    Now $\overline{B}\defined B/\idm B$ is a finite-dimensional $k$-algebra, so the structure theorem for artinian rings gives us that $\spec(\overline{B})$ is finite and discrete, where each of the points is given by the spectrum of a finite-dimensional, local $k$-algebra.
    So by the initial remark, we need to show that $\spec(\overline{B})$ is connected as well.
    Since being henselian is a property that is defined in terms of elements of the ring, we do this using the following characterization of connected components of an affine scheme:
    \begin{fact}
      Let $R$ be a ring.
      Recall that we call an element $e\in R$ \emph{idempotent} if $e^2 = e$ holds.
      There is a bijection
      \begin{align*}
        \lset
        \text{idempotents of $R$}
        \rset
        &
        \smalliso
        \pi_0(\spec(R))
        \\
        e
        &
        \mapsto
        \spec(eR)
      \end{align*}
      Note that the unit element in the ring $eR$ is given by $e$.
    \end{fact}
    Assume now that $\spec(\overline{B})$ is not connected, and let $\overline{e}\in \overline{B}$ be a non-trivial idempotent, with lift $e\in B$.
    Since $B$ is finite over $A$, the element $e$ has an annihilating polynomial, i.e. a monic $f\in A[t]$ such that $f(e) = 0$.
    The $k$-vector space $\overline{B}$ is finite-dimensional, and so $\overline{e}$ has minimal polynomial $\mu_{\overline{e}}\in k[t]$, and in particular, $\mu_{\overline{e}}\divd \overline{f}$.
    As $\overline{e}$ is a non-trivial idempotent element, the minimal polynomial is necessarily given by $\mu_{\overline{e}} = t(t-1)$, and hence $\overline{f} = t^r(t-1)^s \overline{g}$ follows, for a $\overline{g}\in k[t]$ that is coprime to both $t$ and $t-1$. So (again by the Chinese remainder theorem) we get a decomposition
    \[
    \overline{C}\defined k[t]/(\overline{f}) = k[t]/(t^r) \times k[t]/((t-1)^s) \times \overline{C_0}
    \]
    for $\overline{C_0}\defined k[t]/(\overline{g})$.
    Note that we can regard $\overline{C}$ as the reduction of $C\defined A[t]/(f)$ modulo $\idm$.
    This leads to the following factorization
    \begin{equation}
    \label{locring:althensel:idempotentdiag}
    \tag{$\ast$}
    \begin{tikzcd}
      \overline{C}
      \ar{r}
      \ar[twoheadrightarrow]{d}
      &
      \overline{B}
      \\
      k[t]/(t^r)\times k[t]/((t-1)^s)
      \ar[dashed]{ur}
      &
    \end{tikzcd},
    \end{equation}
    where the upper vertical arrow is induced by the map $C = A[t]/(f)\to B$ that is given by mapping $t$ to $e$.
    We're doing all of this because we ultimately want to see that the lift $e\in B$ of the non-trivial idempotent $\overline{e}\in \overline{B}$ is again a non-trivial idempotent, which contradicts our original assumption that $\spec(B)$ is connected.
    Crucial for this is the special case that we've already shown above, which gives us that we can lift idempotents along the projection $C\to \overline{C}$.
    Maybe this schematic is helpful in picturing the lifts we want to do, and what we want to use to obtain them:
    \[
    \begin{tikzcd}
      \overline{B}
      \ar[rightsquigarrow]{r}[above]{\text{\eqref{locring:althensel:idempotentdiag}}}
      &
      \overline{C}
      \ar[rightsquigarrow]{r}[above]{\substack{\text{previous}\\\text{part}}}
      &
      C
      \ar[rightsquigarrow]{r}[above]{\substack{\text{along}\\ C\to B}}
      &
      B
    \end{tikzcd}
    \]
    For the lift of $\overline{e}$ along $\overline{C}$ to $\overline{B}$, we note that the idempotent $t$ in $k[t]/(t(t-1))$ gives rise to an idempotent in $k[t]/(t^r(t-1)^s)$, which gives the lift $\overline{c}\in \overline{C}$ that we want.
    As already mentionend, $\overline{c}$ lifts to an idempotent $c\in C$.
    Since
    \[
    \begin{tikzcd}
      C
      \ar{r}
      \ar{d}
      &
      B
      \ar{d}
      \\
      \overline{C}
      \ar{r}
      &
      \overline{B}
    \end{tikzcd}
    \]
    commutes, we see that the image of $c$ in $B$ is a non-trivial idempotent as well.
    But this contradicts our initial assumption that $\spec(B)$ is connected.
  \end{itemize}
  \par
  Turning to (ii) $\Rightarrow$ (iii), we note that $X = \spec(A)$ is affine, and so both quasi-compact and quasi-separated.
  So we can apply Zariski's main theorem (\cref{defn:zmt}) to the quasi-finite and separated morphism $f\mc Y'\to X$ to obtain a factorization
  \[
  \begin{tikzcd}
    Y'
    \ar[hook]{r}[below]{\text{op. im.}}
    \ar[bend left = 40]{rr}[above]{f'}
    &
    Y
    \ar{r}[above]{f}[below]{\text{finite}}
    &
    X
  \end{tikzcd}.
  \]
  By (ii), we have a decomposition $Y = \coprod_{i=1}^n Y_i$, where $Y_i = \spec(\oo_{Y,y_i})$, where the $\lset y_i\rset$ are the (finitely many) closed poits of $Y$.
  Set now
  \[
  \wtilde{Y}
  \defined
  \coprod_{\lset i\ssp y_i\in Y'\rset}Y_i .
  \]
  As $Y'\sse Y$ is open and the open subset of $Y_i$ that contains $y_i$ is $Y_i$ itself, we have $Y'\cap Y_i = Y_i$, and hence $Y_i \sse Y'$ for all $i$ with $y_i\in Y_i'$.
  Thus $\wtilde{Y}\sse Y'$ is open and closed.
  Let $Y_0 \defined Y' \setminus \wtilde{Y}$ be it's complement, which is open and closed in $Y'$ as well.
  If we can show that $x\notin f'(Y_0)$, then we're done.
  So assume to the contrary that there is a $z\in Y_0$ that maps to $x$, then there is an $i$ with $z\in Y_i \setminus Y_i\setminus \lset y_i\rset$.
  Since the induced morphism $\restrict{f}{Y_i}\mc Y_i \to X$ is finite, its fiber over $\lset x\rset = \spec(k)$ would be zero-dimensional.
  But both $z$ and $y_i$ are contained in it, and since $\overline{\lset y_i\rset} = Y_i \ni z$, the fiber needs to be at least one-dimensional.
  \par
  We continue with the implication (iii) $\Rightarrow$ (iv).
  We can assume that $U$ is affine (\coms can we --- why do the maps agree on the intersections of the covering?\come), and so the morphism $U \to X$ is also separated, and we can apply (iii).
  This reduces the setup to the case where $A\to B$ is a local finite étale $A$-algebra.
  Now $A\to B$ being étale implies that $\idm B$ is the maximal ideal of $B$, and $k = \kappa(\idm) \sse\kappa(\idm B)$ is a finite separable field extension.
  The existence of the lift implies in fact $\kappa(\idm B) = k$, since it induces a map on local rings $B/\idm B \to k$.
  So $B$ is a quotient of $A$ (by Nakayama).
  But since $B$ is also flat over $A$, this implies $B\cong A$ (since we assumed $A$ to be local with maximal ideal $\idm$ in the statement of the proposition).
  \par
  Continuing with (iv) $\Rightarrow$ (v), set $B\defined A[t_1,\ldots,t_n]/(f_1,\ldots,f_n)$ and $Y\defined \spec(B)$ as well as $J\defined \det(\partial_i f_j)\in A[t_1,\ldots,t_n]$.
  The point $x_0$ corresponds to a prime ideal $\idp\sse B$ such that $J$ becomes a unit in $B_{\idq}$, and hence there is even a $b\in B\setminus \idq$ such that $J$ is already a unit in $B[b^{-1}]$ (we get this $b$ as the denominator of the inverse of $J$ in $B_{\idq}$).
  Writing
  \[
  B_b = B[t_{n+1}]/(bt_{n+1}-1) = A[t_{1},\ldots,t_{n+1}]/(f_1,\ldots,f_n , bt_{n+1}-1)
  \]
  and applying \cref{defn:etale-summary-theorem},\ref{defn:etale-summary-theorem:local-jac-crit} gives that $B_{b}$ is étale over $X = \spec(A)$ (the Jacobi determinant of the $n+1$ polynomials is the same as $J$, up to a sign).
  So by (iv), there is a morphism $X\to \spec(B_b)\sse Y$ lifting the morphism $\spec(k)\to Y$ that is defined by $x_0$, which gives the point $x$.
  \par
  The proof of (v) $\Rightarrow$ (vi) involves more polynomials, so we only spell it out briefely:
  If we search for a decomposition $f = gh$ in $A[t]$, then coefficients of $g$ and $h$ determine determine a system of equations.
  Since we have a solution modulo $\idm$, we get a $k$-point on the $A$-scheme cut out by these equations.
  Now the Jacobi-determinant of this equation has a name, it's called the ``resultant'', and modulo $\idm$ this is non-zero.
  So by (v), we can lift this to an $A$-point of the scheme that is cut-out by the equations determining $f$, and hence we get the factorization of $f$.
  \par
  Finally, (vi) $\Rightarrow$ (i) follows from appropriately multiplying with the inverse of a non-constant leading coefficient over $k$.
\end{proof}

\begin{rem}
\label{locring:addrems-henselchar}
\leavevmode
\begin{enumerate}
  \item
  In the decomposition (ii), each of the $B_i$ is itself henselian again:
  If $B_i\to C$ is finite, then $C$ is finite over $A$ too, since the decomposition of $B$ is natural in the sense that the diagram
  \[
  \begin{tikzcd}[row sep = small]
    A
    \ar{r}
    \ar[bend right = 30]{rrdd}
    &
    B
    \ar{r}[above]{\cong}
    &
    B_1\times \ldots \times B_n
    \ar[twoheadrightarrow]{d}
    \\
    &
    &
    B_i
    \ar{d}
    \\
    &
    &
    C
  \end{tikzcd}
  \]
  commutes, and so $A\to C$ is finite, hence $C$ decomposes as in (ii), and each of the factors is also automotically finite over $B_i$.
  \par
  If $A$ is strictly henselian, then each of the $B_i$ are too --- i.e. if $k\defined \kappa(A)$ is separably closed, then all $\kappa_i(B)$ are too:
  Since $A\to B_i$ is finite, $\kappa(B_i)$ is a finite extension of $k$.
  It is now a general fact that finite extensions of separably closed fields are again separably closed (this follows from the multiplicativity of the separable degree).
  \item
  \label{locring:addrems-henselchar:for-affines}
  The conditions of part (iii) of the proposition are in particular satisfied when $Y'\to X$ is an étale morphism of affine schemes.
  But beware that general étale morphisms are not necessarily separated, even when the target is affine!
  One counterexample is given by projection from the line with two origins onto the affine line $\affa^1$.
  \item
  \label{locring:addrems-henselchar:affine-lifting}
  In part \ref{locring:hensel-char:etale-lifting}, the étale scheme $U$ over $X$ is not necessarily affine, i.e. in the proof (iii) $\Rightarrow$ (iv), we get that the result holds for all étale schemes over $X$.
  However, the proof of (iv) $\Rightarrow$ (v) only needed to (iv) to hold for affine étale schemes over $X$.
  Hence showing (iv) in the case of affine étale schemes over $\spec(A)$ suffices to conclude that $A$ is henselian.
  \end{enumerate}
\end{rem}

We now use these insights to better understand henselian rings:

\begin{cor}
  Let $A$ be a henselian ring.
  \begin{enumerate}
    \item
    All quotients of $A$ are henselian.
    \item
    All finite local $A$-algebras are henselian.
  \end{enumerate}
\end{cor}
\begin{proof}
  Part (i) acutally does not need the proposition, and for part (ii) we note that any finite $B$-algebra $B'$ is also finite over $A$, and then \ref{locring:hensel-char:finite-decomp} of \cref{locring:hensel-char} implies the decomposition of $B'$ into local rings, and hence by $B$ is henselian too.
\end{proof}

\begin{cor}
\label{locring:equivalence-for-algebras-over-hens}
  Let $(A,\idm)$ be henselian, with residue field $k$.
  Then base-changing to $k$ induces an equivalence of categories:
  \begin{align*}
    \lset
    \text{finite étale $A$-algebras}
    \rset
    &
    \isomorphism
    \lset
    \text{finite étale $k$-algebras}
    \rset
    \\
    B
    &
    \longmapsto
    B\tensor_A k
  \end{align*}
\end{cor}
\begin{proof}
  For essential surjectivity, we use the structure theorem of finite-dimensional étale $k$-algebras (\cref{defn:fd-etale-over-field}) to see that it suffices to prove that finite-dimensional separable field extensions of $k$ are in the essential image.
  So let $L$ be a separable field extension of $k$.
  By \cref{defn:produce-stand-etale}, we can find a standard étale $A$-algebra $A\to B$ and a prime ideal $\idp\sse B$ such that $\kappa(\idp) = L$ and $\idp \cap A = \idm$ (note that we do not need that $A$ is henselian for that).
  In the decomposition as in (iii) of \cref{locring:hensel-char} (c.f. \cref{locring:addrems-henselchar},\ref{locring:addrems-henselchar:for-affines}), we have that one of the local rings satisfies $B_{\idq} = B_i$.
  So $R\to A_i$ is finite and étale, and reduces modulo $\idm$ to $k\to L$ --- the functor is indeed essentially surjective!\par
  Turning towards fully-faithfullness, we want to show that the map
  \[
  \hom_A(B,B')
  \to
  \hom_k(B\tensor_A k, B'\tensor_A k)
  \]
  is an isomorphism for all finite étale $A$-algebras $B,B'$.
  Since everything in this map commutes with finite products, part (ii) of \cref{locring:hensel-char} tells us that we can assume that both $B$ and $B'$ are local.
  In particular, we can assume that $\spec(B')$ is connected.
  For fullness, we first note that every $\alpha\mc B\tensor_A k\to B'\tensor_A k$ induces a map $\alpha_1\mc B\to B'\tensor_A k$ (by precomposing with the projection $B\twoheadrightarrow B\tensor_A k$), which in turn induces a map $\alpha_2 \mc B'\tensor_A B \to B'\tensor_A k$ (the base change of $\alpha_1$ to $B'$ over $A$).0
\end{proof}

\begin{cor}[Hensel's Lemma]
  Let $(A,\idm)$ be a $\idm$-adically complete local ring, with residue field $k$.
  Then $A$ is henselian.
\end{cor}
\begin{proof}
  By part \ref{locring:hensel-char:etale-lifting} of \cref{locring:hensel-char} it suffices to show that if $B$ is an étale $A$-algebra with $s_0\mc B\to k$ a $k$-rational point of $\spec(B)$, then $s_0$ lifts to a section $s\mc B\to A$ (this is enough, c.f. \cref{locring:addrems-henselchar},\ref{locring:addrems-henselchar:affine-lifting}).
  Since we assume $A$ to be $\idm$-adically complete, we have $A = \varprojlim A/\idm^{n+1}$, and hence it is enough to construct compatible sections $s_n\mc B\to A/\idm^{n+1}$ for all $n\geq 0$.
  We already have a section $s_0$, and we can construct the other sections inductively as the unique lifts in
  \[
  \begin{tikzcd}
    \spec(A_{n})
    \ar[closed, hook]{d}
    \ar{r}[above]{s_n}
    &
    \spec(B)
    \ar{d}
    \\
    \spec(A_{n+1})
    \ar{r}
    \ar[dashed]{ur}[below right]{\exists !}
    &
    \spec(A)
  \end{tikzcd},
  \]
  which exists since $A\to B$ is étale.
\end{proof}

\subsection{Henselization}

\begin{prop}
\label{locrings:henselization}
  Let $(A,\idm)$ be a local ring with resiude fiel $k$.
  Fix an embedding $\eta\mc k\hookrightarrow k\sepclos$ into a separable closure.
  Then there exists a unique commutative diagram of the form
  \[
  \begin{tikzcd}
    A
    \ar{r}
    \ar{d}
    &
    A\hnsl
    \ar{d}
    \ar{r}
    &
    A\shnsl
    \ar{d}
    \\
    k
    \ar{r}[below]{=}
    &
    k
    \ar{r}[below]{\eta}
    &
    k\sepclos
  \end{tikzcd}
  \]
  that has the following properties:
  \begin{enumerate}
    \item
    The morphisms $A\to A\hnsl$ and $A\hnsl \to A\shnsl$ are local morphisms.
    \item
    The ring $A\hnsl$ is henselian; the ring $A\shnsl$ is strictly henselian.
    \item
    Both $A\hnsl$ and $A\shnsl$ are filtered colimits of étale $A$-algebras.
    \item
    The maximal ideal of $A\hnsl$ is given by $\idm A\hnsl$; the maximal ideal of $A\shnsl$ is given by $\idm A\shnsl$.
    \item
    The residue field of $A\hnsl$ is $k$; the residue field of $A\shnsl$ is $k\sepclos$.
  \end{enumerate}
\end{prop}

\begin{proof}
Let $x\in \spec(A)$ be the maximal ideal, and denote by $\nvcataff_{x} \sse \nvcat_x$ respectively $\ncataff_{\overline{x}}\sse \ncat_{\overline{x}}$ the associated subcategories of the categories na\"{i}ve-étale respectively étale neighborhoods where the étale $X$-scheme $U\to X$ is affine (c.f. \cref{locrings:canonical-etale-nhood-cat} for terminology in the étale case).
Both of them are cofiltered (\cref{locrings:etale-nhoods-are-cofiltered}).
We now set
\[
A\hnsl
\defined
\colim_{(U,u)\in \nvcataff_x}
\Gamma(U,\oo_U)
~~
\text{and}
~~
A\shnsl
\defined
\colim_{(U,\overline{u})\in \ncataff_{\overline{x}}}
\Gamma(U,\oo_U).
\]
Since the $U$ are étale over $\spec(A)$, the colimits are indeed over étale algebras.
Also, note that we could have equivalently set
\[
A\hnsl
\defined
\colim_{(U,u)\in \nvcataff_x}
\oo_{U,u}
~~
\text{and}
~~
A\shnsl
\defined
\colim_{(U,\overline{u})\in \ncataff_{\overline{x}}}
\oo_{U,u},
\]
since the stalks are themselves colimits over affine open neighborhoods, and these are étale over $X$ again.
In particular, both $A\hnsl$ and $A\shnsl$ are local rings (they are filtered colimits of local rings along local ring maps), and the canonical morphisms $A\to A\hnsl$ and $A\to A\shnsl$ are thus local morphisms.
Concerning the residue fields, we recall that all local rings $\oo_{U,u}$ for $(U,u)\in \nvcat_x$ has residue field $k$, and hence $A\hnsl$ does too.
For $A\shnsl$, we have that the residue field of $\oo_{U,u}$ for $(U,\overline{u})\in \ncat_{\overline{x}}$ is a finite separable extension of $k$, and hence $\kappa(A\shnsl)\sse k\sepclos$.
For the other inclusion, we need that every finite separable field extension of $k$ occurs as the residue field of some $\oo_{U,u}$ --- but we already know this, from the equivalence of categories in \cref{locring:equivalence-for-algebras-over-hens}.
We still need to show that $A\hnsl$ is henselian and $A\shnsl$ is strictly henselian.
This will be the same for $A\hnsl$ and $A\shnsl$, and we already know that $A\shnsl$ has separably closed residue field.
We use \cref{locring:hensel-char},\ref{locring:hensel-char:etale-lifting} for that, and by \cref{locring:addrems-henselchar},\ref{locring:addrems-henselchar:affine-lifting}, it suffices to consider étale algebras $A\hnsl$-algebras $A\hnsl\to S$.
In particular, any such $S$ is finitely represented over $A\hnsl$.
Using the process explained in \cref{app:limits:base-change-for-filtered}, we can write $\pphi\mc A\hnsl \to S$ as a directed colimit
\[
\left(
\pphi
\mc
A\hnsl
\to
S
\right)
=
\colim_{\lambda \in \Lambda}
\left(
A_{\lambda}
\to
S_{\lambda}
\right),
\]
where $A_{\lambda} = \Gamma(U_{\lambda},\oo_{U_{\lambda}})$ for some (na\"{i}ve-) étale neighborhood $U$ of $x$, $\Lambda\sse \nvcataff_{x}$ is a directed subcategory, and for every $\mu \geq \lambda$, the map $\pphi_{\mu}$ is the base change of $\pphi_{\lambda}$.
By \cite{stacks}*{\stackstag{07RP}}, we can assume that all of the $\pphi_{\lambda}$ are étale.
After describing this setup, let us return to showing that we have the lifting property for $S$.
Assume we have a lift $\spec(k)\to \spec(S)$ of the map $\spec(k)\to \spec(A)$.
Then we can write this a system of compatible maps $\spec(k)\to \spec(S_{\lambda})$.
Precomposing with the map $\spec(k\sepclos)\to \spec(k)$ gives a system of maps $\overline{s_{\lambda}}\mc \spec(k\sepclos)\to \spec(S_{\lambda})$, and each of these maps is an étale neighborhood of $\overline{x}$, such that underlying point of $\overline{s_{\lambda}}$ is $k$.
So each of the $S_{\lambda}$ occurs as some of the $A_{\mu}$ in the colimit description.
This gives a morphism
\[
S = \colim_{\lambda\in \Lambda^{+}}S_{\lambda}
\to
\colim_{\mu \in \nvcataff_{x}} A_{\mu} = A,
\]
which is the extension of the section we wanted to construct.
\end{proof}
\coms TODO: need to add the verifications of the properties of the étale neighborhoods for that, i.e. that they are finitely presented over $A\hnsl$ and that we can apply the stacks lemma to get the étale-after-some-step conclusion.
Also, I should check that this is indeed a section, bc i'm not too sure that that's right, but only meta-sure (but I'm too feed-up with étale nhoods for the moment).
\come
\begin{defn}
  We call the ring $A\hnsl$ from \cref{locrings:henselization} the \emph{henselization} of $A$; similarly, the ring $A\shnsl$ is called the \emph{strict henselization} of $A$.
  \glsadd{henselization}
  \glsadd{strict-henselization}
\end{defn}
\begin{prop}
  The map $A\to A\hnsl$ is the universal map from $A$ into a henselian local ring, i.e.  other local maps $A\to C$ factor over $A\hnsl$:
  \[
  \begin{tikzcd}
    A
    \ar{r}
    \ar{d}
    &
    C
    \\
    A\hnsl
    \ar[dashed]{ur}[below right]{\exists !}
  \end{tikzcd}
  \]
  The similar statement holds for $A\shnsl$.
\end{prop}
\begin{proof}
  We will show the claim for $A\shnsl$.
  Let $A\to S$ be a local morphism into a strictly henselian ring $S$, together with a morphism $k\sepclos \to \kappa(S)$.
  We need to construct (compatible) morphisms $A_{\lambda}\to S$ for all $A_{\lambda} = \Gamma(U,\oo_U)$ with $(U,\overline{u})$ an étale neighborhood of $\overline{x}$.
  Now the composition $\kappa(A_{\lambda})\to k\sepclos \to \kappa(S)$ base-changes to a morphism $A_{\lambda}\tensor_A S \to \kappa(S)$, and since $S$ is henselian, this lifts to a unique $S$-algebra morphism $A_{\lambda}\tensor_A S\to S$ (\cref{locring:hensel-char},\ref{locring:hensel-char:etale-lifting}).
  Under the adjunction
  \[
  \hom_{S\algcat}(A_{\lambda}\tensor_A S, S)
  \cong
  \hom_{A\algcat}(A_{\lambda},S),
  \]
  this gives us the $A$-algebra morphism $A_{\lambda}\to S$ we've been looking for.
  Since everything is functorial and natural, we should get that the morphisms $A_{\lambda}\to S$ are indeed compatible with taking the colimit over $\ncat_{\overline{x}}$, and that the colimit of these morphisms is unique.
\end{proof}
\begin{example}
  \leavevmode
  \begin{enumerate}
    \item
    Let $A$ be a field and $A\defined A[t_1,\ldots,t_n]_{(t_1,\ldots,t_n)}$.
    Then the henselization of $A$ is given as:
    \[
    A\hnsl
    =
    \lset
    f\in k\llbracket t_1,\ldots,t_n\rrbracket
    \ssp
    \text{$f$ is algebraic over $A$}
    \rset.
    \]
    \item
    Let $K$ be a number field with ring of integers $\ook$.
    Let $\idp \sse \ook$ be a prime ideal and $A\defined \oo_{K,\idp}$ the associated local ring.
    Denote by $K\sepclos$ the separable closure of $K$, with ring of integers $\oo_{K\sepclos}$, and let $\overline{p}\sse \oo_{K\sepclos}$ be some lift of $\idp$.
    The absolute Galois group $\gal(K\sepclos/K)$ acts on $\oo_{K\sepclos}$, and associated to it is the \emph{decomposition group}:
    \[
    D_{\overline{\idp}/\idp}
    \defined
    \lset
    \sigma \in \gal(K\sepclos/K)
    \ssp
    \sigma(\overline{\idp})
    =
    \overline{\idp}
    \rset.
    \]
    Then the henselization of $A$ is given by the localization of the ring of fixed points $\left(\oo_{K\sepclos,\overline{\idp}}\right)^{D_{\overline{\idp}/\idp}}$ at the ideal $\overline{\idp}^{D_{\overline{\idp}/\idp}}$.
    \coms
    (this needs a reference and details)
    \come
  \end{enumerate}
\end{example}
