\subsection{Cohomology of $\gmult$}
\begin{lem}
\label{cohomology:lerray-for-different-sites}
  Let $X$ be a scheme $\shz\in \shcat(\zar{X})$ a sheaf on the Zariksi site of $X$.
  \begin{enumerate}
  \item
  Then the assignemt $U\mapsto \Gamma(U,\inverseimage{f}\shz)$ for $U\to X$ étale is a sheaf on $\et{X}$, which we denote by $W(\shz)$.
  \item
  \label{cohomology:lerray-for-different-sites:cohomology}
  Let $F\mc \et{X}\to \zar{X}$ be the morphism of sites that is induced by the identity on $X$.
  Then there is a spectral sequence
  \[
  E_2^{p,q}
  =
  \hh^p(\zar{X},\rderived^q\directimage{F}W(\shz))
  \Rightarrow
  \hh^{p+q}(\et{X},W(\shz)).
  \]
  In particular, if $\rderived^q\directimage{F}W(\shz) = 0$ holds for a $q$, then the $q$-th Zariski and étale cohomology of $\shz$ agree up to $q$, i.e. it holds that there are natural isomorphisms
  \[
  \hh^n(\zar{X},\shz)
  \isomorphism
  \hh^n(\et{X},W(\shz)).
  \]
  for $n\leq q$.
  \end{enumerate}
\end{lem}
\begin{proof}
  We basically only need to prove (ii).
  For that, note that we have the following commutative diagrams of functors:
  \[
  \begin{tikzcd}
    \shetcat{X}
    \ar{r}[above]{\directimage{F}}
    \ar{rd}[below left]{\Gamma(\et{X},-)}
    &
    \shcat(\zar(X))
    \ar{d}[right]{\Gamma(\zar{X},-)}
    \\
    &
    \abcat
  \end{tikzcd}
  \]
  The functor $\directimage{F}$ is left-exact and preserves injective objects (since having the lifting property with respect to all étale morphism over $X$ is stronger than having it only with respect to all open immersions).
  Then this is a special instance of the Grothendieck spectral sequence.
  The second result follows the vanishing on the diagonal region above $\hh^q(\zar{X},W(\shz))$.
\end{proof}
\begin{rem}
  Note that part (ii) from above is in fact valid for all sheaves on $\et{X}$.
\end{rem}
\subsubsection*{Hilbert's Theorem 90 and $\hh^1(\et{X},\gmult)$}
\begin{theorem}
\label{multscheme:hilbert90}
  Let $X$ be a scheme.
  Then there are natural isomorphisms
  \[
  \hh^1(\zar{X},\ox\unit)
  \isomorphism
  \hh^1(\et{X},\gmult).
  \]
\end{theorem}
\begin{proof}
  By \cref{cohomology:lerray-for-different-sites},\ref{cohomology:lerray-for-different-sites:cohomology}, it suffices to show $\rderived^1\directimage{F}\gmult = 0$, where $F\mc \et{X}\to \zar{X}$ is the morphism of sites induced by the identity on $X$.
  We can describe the sections of the derived pushforward explicitly --- for $U\sse X$ zariski-open, we have
  \[
  \Gamma(U,\rderived^1\directimage{F}\gmult)
  =
  \hh^1(\et{X},\gmult).
  \]
  So it's enough to show that for every $U\sse X$ zariski-open and $s\in \hh^1(\et{U},\gmult)$ there is a Zariski cover $U = \cup U_i$ such that $\restrict{s}{\etindex{U}{i}} = 0$, and in fact it suffices to show this for local rings, and by \cref{cechcoh:first-cohs-agree} it suffices to show this for \v{C}ech cohomology.
  So we're left with showing the following:
  \begin{reduction}
    For every local ring $A$ with $V\defined \spec(A)$, it holds that
    \[
    \chhlgy^1(\et{V},\gmult) =  0.
    \]
  \end{reduction}
  By the definition of the absolute \v{C}ech cohomology as colimit, $\alpha$ comes from a $\alpha'\in \chhlgy^1(W/V,\gmult)$, where $W\to V$ is étale, surjective, finite-type map in $W$ is an affine scheme, and in particular, it is represented by a $\pphi \in \gmult(W\times_V W)$.
  Since we're in the case of affine schemes, we can regard $\gmult(W\times_V W)$ as the group of units of the algebra $B\tensor_A B$, and so we can regard it as an isomorphism of $B\tensor_A B$-modules:
  \[
  \pphi \mc N\tensor_A B \smalliso B\tensor_A N
  \]
  where we write $N$ for the $B$-module $B$.
  Now $\pphi$ is a \v{C}ech one-cocycle, and the explicit description of the \v{C}ech differential implies that $\pphi$ satisfies the cocyle condition for descent data.
  Since $A\to B$ is in particular faithfully flat, flat descent (\cref{defn:flat-descent}) implies that there is an $A$-module $M$ such that $N = M\tensor_A B$.
  Since $N$ is flat and finitely generated over $B$, the same holds for $M$ over $A$ (c.f. \cref{defn:descent-for-projective}).
  Now $A$ is local, so $M$ is a finitely generated free $A$-module.
  This implies that $\pphi$ is in fact the identity (or like multiplication with the unit element), and so it's in fact a \v{C}ech-coboundary, so $\alpha' = 0$ in $\chhlgy^1(W/V,\gmult)$.
\end{proof}

\begin{rem}
  The same argument shows that $\chhlgy^1(\et{X},\genlin_n) = 0$ for all $n$.
\end{rem}

\begin{cor}[Hilbert's theorem 90]
  Let $K\sse L$ be a Galois extension.
  Then
  \[
  \hh^1(\gal(L/K),L\unit) = 0.
  \]
\end{cor}
\begin{proof}
  Consider first the case the $L$ is separably closed, so we have the situation of $k\sse k\sepclos$.
  Let $X = \spec(k)$ and let $\overline{x}\mc \spec(k\sepclos)\to X$ be the corresponding geometric point.
  We have $k\sepclosunit \cong \left(\oo_{\et{X},\overline{x}}\shnsl\right)\unit$;
  moreover, using \cref{cohomology:group-cohomology-as-galois-cohomology}, we can identify the group cohomology with values in the stalk with the étale cohology.
  Combining this with the theorem, we have
  \[
  \hh^1(\gal(k\sepclos,k),k\sepclosunit)
  \cong
  \hh^1(\et{X},\oo_{X}\unit)
  \cong
  \pic(X).
  \]
  But since $k$ is a field, there are no non-trivial line bundles over $\spec(k)$, and hence $\pic(X)$ is trivial.
  \par
  For the general case, let $G\defined \gal(L\sepclos/K)$.
  Then $H = \gal(L\sepclos/L)\sse G$ is closed, and $G/H\cong \gal(L/K)$.
  Combining the Hochschild-Serre spectral sequence with the relation between group cohomology and étale cohomology once more gives us a spectral sequence
  \[
  E_2^{p,q}
  =
  \hh^p(G/H,\hh^q(H,L\sepclosunit))
  \Rightarrow
  \hh^{p+q}(G,L\sepclosunit).
  \]
  In it, $E_2^{0,1} = 0$, since $\hh^1(H,L\sepclosunit) = 0$ by the consideration above of separably closed fields.
  Hence
  \[
  \hh^1(\gal(L/K),L\unit)
  =
  E_{2}^{1,0}
  \cong
  \hh^1(G,L\sepclosunit) = 0.
  \]
\end{proof}

\subsubsection*{Interlude on Divisors}
\begin{defn}
  Let $X$ be a normal and connected scheme.
  A \emph{prime divisor} of $X$ is a closed integral subscheme of codimension one.
  The free abelian group on the set of prime divisors is denoted by $\wdiv(X)$.
\end{defn}
\begin{lem}
  Let $X$ be a normal and connected scheme.
  \begin{enumerate}
    \item
    The assignement $U\mapsto \wdiv(U)$ for $U\sse X$ open affine defines a sheaf on $\zar{X}$.
    \item
    Let $U\sse X$ be a non-empty open subset of $X$.
    Then the map
    \begin{align*}
    \lset
    \text{prime divisors of $X$ meeting $U$}
    \rset
    &
    \longrightarrow
    \lset
    \text{prime divisors of $U$}
    \rset
    \\
    Z
    &
    \longmapsto
    U\cap Z
    \intertext{If $U = \spec(A)$ is an open affine, then this induces a bijection}
    \lset
    \text{height $1$ prime ideals of $A$}
    \rset
    &
    \longrightarrow
    \text{prime divisors of $U$}
    \\
    \idp
    &
    \longmapsto
    V(\idp).
    \end{align*}
    \item
    Let $K$ be the field of rational functions on $X$, i.e. the common fraction field of all $\Gamma(U,\ox)$ for $U\sse X$ open affine.
    Then every prime divisor $Z$ on $X$ defines a discrete valution $\nu_Z$ on $K$.
  \end{enumerate}
\end{lem}

\begin{prop}
  Let $X$ be a normal and connected scheme.
  There is a sequence of sheaves on $\zar{X}$ of the form
  \[
  \begin{tikzcd}[column sep = small, cramped]
    0
    \ar{r}
    &
    \ox\unit
    \ar{r}
    &
    K\unit
    \ar{r}
    &
    \wdiv
    \ar{r}
    &
    0
  \end{tikzcd}
  \]
  where $K$ is the field of rational functions on $X$.
  This sequence is always left-exact; if $X$ is regular, then it is exact.
\end{prop}
\begin{proof}
  It suffices to show this on stalks, in which case the sequence reduces to
  \[
  \begin{tikzcd}[column sep = small]
    0
    \ar{r}
    &
    \oxx\unit
    \ar{r}
    &
    K\unit
    \ar{r}[above]{\nu_x}
    &
    \displaystyle
    \zz
    \ar{r}
    &
    0
  \end{tikzcd},
  \]
  where $K=\fracfield(\oxx)$ holds, and $\nu_x$ is the valuation of the DVR $\oxx$ (since $\oxx$ is an integrally closed local ring).
  By the description of the units in $\oxx$ in terms of the valuation, this is a complex.
  It is surjective if $\oxx$ is regular, i.e. if $X$ is a regular scheme.
\end{proof}
Similarly, one can show (\coms Milne claim's in his script that it follows from the zariski case, but i don't get that\come)
\begin{prop}
  Let $X$ be a connected and normal scheme.
  Then there is a sequence of sheaves on $\et{X}$ of the form
  \[
  \begin{tikzcd}[column sep = small, cramped]
  0
  \ar{r}
  &
  \gmult
  \ar{r}
  &
  \directimage{j}\gmult_{,K}
  \ar{r}
  &
  \displaystyle
  \bigoplus_{\codim(z) = 1}
  i_{z,\ast}
  \zz
  \ar{r}
  &
  0
  \end{tikzcd},
  \]
  where $j$ is the inclusion of the generic point, i.e. $j \mc \eta = \spec(K)\hookrightarrow X$.
  This sequence is always left-exact; it is right-exact if $X$ is regular.
\end{prop}

\needspace{3\baselineskip}
\subsubsection*{The étale cohomology of a curve with coefficients in $\gmult$}
\begin{theorem}
	\label{multscheme:curve-gmult}
  Let $X$ be a smooth connected curve over a separably closed field $k$.
  Then
  \[
  \hh^i(\et{X},\gmult)
  \cong
  \begin{cases}
    \Gamma(X,\ox)\unit
    &
    \text{if $i=0$,}
    \\
    \pic(C)
    &
    \text{if $i=1$,}
    \\
    0
    &
    \text{otherwise.}
  \end{cases}
  \]
\end{theorem}
\begin{rem}
    We already know from \cref{multscheme:hilbert90} that the first cohomology group of $\et{X}$ is isomorphic to it's Picard group.
    What is new now is the vanishing of the higher cohomology groups.
    The proof we're about to does not really incorperate Hilbert 90 however.
\end{rem}
\begin{proof}
  Let $\eta\mc \spec(K)\to X$ be the generic point of $X$.
  We will black-box for now the following result on the cohomology of $\eta$, which is in fact a statement about the Galois cohomology of $K\sse K\sepclos$.
  \begin{fact}
	  \label{multscheme:curve-gmult:galois}
    It holds that $\hh^n(\et{\spec(K)},\gmult_{,\spec(K)}) = 0$ for $n\geq 1$.
  \end{fact}
\end{proof}

\subsubsection*{The étale cohomology of a curve with coefficients in $\mu_n$ and $\zz/n\zz$}

\begin{prop}
\label{multscheme:curve-mun}
	Let $X$ be a smooth projective curve of genus $g$ over an algebraically closed field $k$ and $n\geq 1$ invertible in $k$.
	Then there are canonical identifications
	\[
		\hh^q(\et{X},\mu_n)
		=
		\begin{cases}
			\mu_n(k)
			&
			\text{if $q$ = 0,}
			\\
			\pic(X)[n]
			&
			\text{if $q = 1$,}
			\\
			\zz/n\zz
			&
			\text{if $q = 2$,}
			\\
			0
			&
			\text{if $q \geq 3$.}
		\end{cases}
	\]
\end{prop}
\begin{rem}
	Since there is a non-canonical identification $\mu_n \cong \underline{\zz/n\zz}$ and it also holds that $\pic^0(X)[n] \cong \left(\zz/n\zz\right)^{\oplus 2g}$, the above propostion implies

	\[
		\hh^q(\et{X},\underline{\zz/n\zz})
		=
		\begin{cases}
			\zz/n\zz
			&
			\text{if $q$ = 0,}
			\\
			\left(\zz/n\zz\right)^{\oplus 2g}
			&
			\text{if $q = 1$,}
			\\
			\zz/n\zz
			&
			\text{if $q = 2$,}
			\\
			0
			&
			\text{if $q \geq 3$.}
		\end{cases}
	\]
\end{rem}

\begin{proof}[Proof of \cref{multscheme:curve-mun}]
	Recall that we have the Kummer sequence \eqref{etsheaves:kumm-sequence}:
	\[
		\begin{tikzcd}[column sep = small, cramped]
		0
		\ar{r}
		&
		\mu_n
		\ar{r}
		&
		\gmult
		\ar{r}[above]{(-)^n}
		&
		\gmult
		\ar{r}
		&
		0
		\end{tikzcd}.
	\]
	Plugging in the result on the cohomology of $X$ with coefficients in $\gmult$ from \cref{multscheme:curve-gmult}, the associated long-exact cohomology sequence reads as:
	\[
		\begin{tikzcd}
		0
		\ar{r}
		&
		\mu_n(k)
		\ar{r}
		&
		k^{\ast}
		\ar{r}
		\ar[draw = none]{d}[name = X, anchor = center]{}
		&
		k^{\ast}
		\ar[rounded corners,
	            to path={ -- ([xshift=2ex]\tikztostart.east)
	                      |- (X.center) \tikztonodes
	                      -| ([xshift=-2ex]\tikztotarget.west)
	                      -- (\tikztotarget)}]{dll}[at end]{}
		\\
		&
		\hh^1(\et{X},\mu_n)
		\ar{r}
		&
		\picscheme(X)
		\ar{r}
		\ar[draw = none]{d}[name = Y, anchor = center]{}
		&
		\picscheme(X)
		\ar[rounded corners,
	            to path={ -- ([xshift=2ex]\tikztostart.east)
	                      |- (Y.center) \tikztonodes
	                      -| ([xshift=-2ex]\tikztotarget.west)
	                      -- (\tikztotarget)}]{dll}[at end]{}
		\\
		&
		\hh^2(\et{X},\mu_n)
		\ar{r}
		&
		0
		\ar{r}
		&
		\ldots
		\end{tikzcd}
	\]
	
	Since $k$ is algebraically closed, the $n$-th power map is surjective, and so we need to compute the kernel and cokernel of the map $\pic(X)\xrightarrow{(-)^n}\pic(X)$.
	The kernel is precisely given by the $n$-torsion of $\pic(X)$, i.e. $\pic(X)[n]$.
	To get to the description of the second cohomology group, we need some magic.
	Denote by $\pic^0(X)$ the kernel of the surjective degree map $\pic(X)\to \zz$.
	Then there is a commutative diagram
	\[
		\begin{tikzcd}
		0
		\ar{r}
		&	
		\picscheme^0(X)
		\ar{r}
		\ar{d}[right]{(-)^n}
		&
		\picscheme(X)
		\ar{r}
		\ar{d}[right]{(-)^n}
		&
		\zz
		\ar{r}
		\ar{d}[right]{\cdot n}
		&
		0
		\\
		0
		\ar{r}
		&
		\picscheme^0(X)
		\ar{r}
		&
		\picscheme(X)
		\ar{r}
		&
		\zz
		\ar{r}
		&
		0
		\end{tikzcd}
	\]
	and the induced map $\pic^0(X) \xrightarrow{(-)^n} \pic^0(X)$ is surjective.
	Applying the snake lemma to this diagram gives the result, as it gives a canoncial identification
	\[
		\coker\left(\pic(X) \xrightarrow{(-)^n} \pic(X)\right)
		\cong
		\zz/n\zz.
	\]
\end{proof}


\begin{prop}
	\label{multscheme:affine-curve}
	Let $X$ be a smooth projective curve of genus $g$ over an algebraically closed field $k$ and $n\geq 1$ invertible in $k$.
	Let $j\mc U \hookrightarrow X$ be an open immersion.
	Then 
	\[
		\hh^q(\et{U},\underline{\zz/n\zz})
		=
		\begin{cases}
			\zz/n\zz		
			&
			\text{if $q=0$;}
			\\			
			\pic(X)[n]
			\oplus
			\left(\zz/n\zz\right)^{r-1}
			&
			\text{if $q = 1$};
			\\
			0
			&
			\text{if $q \geq 2$,}
		\end{cases}
	\] 
	where $r\defined \abs{X \setminus U}$.
\end{prop}
The proof we are about to give is largely a copy of \cite{pdisk}.
To deduce the proposition from the cohomology of $X$, we need the following excision result (which will probably move to a different place in these notes when I have more time):

\begin{prop}
	\label{multscheme:exc-seq}
	Let $X$ be a scheme, and $i\mc Z \to X$ a closed immersion, with complement $j\mc U \to X$.
	For any sheaf $\shf$ on $\et{X}$, there is a natural long-exact sequence of the form:
	\[
		\begin{tikzcd}
		0
		\ar{r}
		&
		\Gamma(\et{Z},i^{!}\shf)
		\ar{r}
		&
		\shf(X)
		\ar{r}
		\ar[draw = none]{d}[name = X, anchor = center]{}
		&
		\shf(U)
		\ar[rounded corners,
	            to path={ -- ([xshift=2ex]\tikztostart.east)
	                      |- (X.center) \tikztonodes
	                      -| ([xshift=-2ex]\tikztotarget.west)
	                      -- (\tikztotarget)}]{dll}[at end]{}
		&
		\\
		&
		\hh^1_Z(\et{X},\shf)
		\ar{r}
		&
		\hh^1(\et{X},\shf)
		\ar{r}
		&
		\hh^1(\et{U},\shf)
		\ar{r}
		&
		\ldots
		\end{tikzcd}
	\]
	
\end{prop}

\begin{prop}
	\label{multscheme:excision}
	Let $Z \sse X$ and $Z' \sse X'$ be closed subschemes and let $\pi \mc X' \to X$ be an étale morphism.
	Assume that $\pi(X' \setminus Z') \sse X \setminus Z$ and that the restricted map is an isomorphism $\restrict{\pi}{Z'} \mc Z' \isomorphism Z$.
	Then the map $\hh_Z^q(X,\shf) \to \hh_Z^q(X',\inverseimage{\pi}\shf)$ is an isomorphism for all $q \geq 0$ and $\shf \in \shetcat{X}$.
\end{prop}
A proof of these propositions can be found in \cite{milne}*{III. Prop. 1.25/1.27}.
Maybe it would be interesting to see how this relates to the étale Mayer-Vietoris, but I don't know that.
%TODO: Etale exc./mv?
\begin{proof}[Proof of \cref{multscheme:affine-curve}]
	Set $Z \defined X\setminus U$.
	Since this is a finite collection of disjoint points (\coms Is it? I dunno\come), it suffices to show the case where $Z$ consists only of one point, say $Z = \lset z \rset$ (because relative cohomology is additive with respect to disjoint unions); hence the proof is ``just'' a calculation of cohomology with support in a point.
	We will do this in several steps\footnote{Note that we omit most of the notational references to étale sites of the various schemes involved, because otherwise this will look really unpleasing. This should be ok, since there will be no Zariski sites involved in this proof.}:
	\begin{enumerate}
		\itshape
		\item
		\label{multscheme:affc:to-line}
			We have the following alternative descriptions of $\hh^i_z(\et{X},\zz/n\zz)$:
			\[
				\hh^i_z(\et{X},\zz/n\zz)
				\cong 
				\hh^i_z(\spec(\oo\shnsl_{X,z}),\zz/n\zz)
				\cong 
				\hh^i_0(\spec(\oo\shnsl_{\affa^1_k,0}),\zz/n\zz);
			\]
			where the last isomorphism is induced by an isomorphism of schemes.
		\item 
		\label{multscheme:affc:to-gal}
			Let $K \defined \fracfield(\oo\shnsl_{\affa^1_k,0})$.
			It holds that $\hh^1_0(\spec(\oo\shnsl_{\affa_k^1,0}),\zz/n\zz) = 0$ and for $i \geq 2$:
			\[
				\hh^i_0(\spec(\oo\shnsl_{\affl,0}),\zz/n\zz)
				\cong
				\hgal^{i-1}(\gal(K\sepclos/K),\zz/n\zz).
			\]
		\item 
		\label{multscheme:affc:galcomp}
			It holds that $\gal(K\sepclos/K) \cong \widehat{\zz}$, and 
			\[
				\hgal^q(\widehat{\zz},\zz/n\zz)
				\cong
				\begin{cases}
					\zz/n\zz
					&
					\text{if $q=0,1$;}
					\\
					0
					&
					\text{otherwise.}
				\end{cases}
			\] 
	\end{enumerate}
	Before we actually do this, let us show how they imply the proposition:
	Combining \ref{multscheme:affc:to-line}, \ref{multscheme:affc:to-gal} and \ref{multscheme:affc:galcomp} gives 
	\[
		\hh_Z^i(\et{X},\zz/n\zz)	
		\cong
		\begin{cases}
			\left(\zz/n\zz\right)^{\oplus r}
			&
			\text{if $i = 2$;}
			\\
			0
			&
			\text{otherwise.}
		\end{cases}
	\] 
	Inserting this and the results from \cref{multscheme:curve-mun}, into the long-exact sequence of \cref{multscheme:exc-seq} results in an exact sequence of the form 
	\[
		\begin{tikzcd}
		\ldots
		\ar{r}
		&
		0
		\ar{r}
		&
		\picscheme(X)[n]
		\ar{r}
		\ar[draw = none]{d}[name = X, anchor = center]{}
		&
		\hh^1(\et{U},\zz/n\zz)
		\ar[rounded corners,
	            to path={ -- ([xshift=2ex]\tikztostart.east)
	                      |- (X.center) \tikztonodes
	                      -| ([xshift=-2ex]\tikztotarget.west)
	                      -- (\tikztotarget)}]{dll}[at end]{}
		&	      
		\\
		&
		\left(\zz/n\zz\right)^{\oplus r}
		\ar{r}
		&
		\zz/n \zz
		\ar{r}
		&
		0
		\ar{r}
		&
		\ldots
		\end{tikzcd}
	\]
	\coms I have no clue why this splits \come.
	We also see that all cohomology groups above $i=2$ have to vanish, and since $U$ is connected, we have $\Gamma(\et{U},\underline{\zz/n\zz}) = \zz/n\zz$ which finishes the proof of the proposition.
	\par 
	To get the first isomorphism of \emph{Step \ref{multscheme:affc:to-line}}, we first note that the system $\Lambda^0$ of quasi-compact étale neighborhoods of $z$ that are affine and for which the fiber of $z$ consists only of one point is cofinal in the category of all étale neighborhoods of $z$ in $X$.
	For every such $V_j\in \Lambda^0$ with $v_j$ the unique preimage of $z$, the complement $V_j \setminus \lset v_j \rset$ is quasi-compact too.
	The excision statement of \cref{multscheme:excision} gives natural isomorphisms
	\[
		\hh^q_z(\et{X},\zz/n\zz)
		\isomorphism
		\hh^q_{v_j}(\etindex{V}{j},\zz/n\zz)
	\]
	for every such pair $(V_j,v_j)$ with $V_j \in \Lambda_0$.
	Taking the colimit over all these pairs gives the first isomorphism, since it commutes with taking cohomology by \cref{cohomology:commutes-with-inverse-limits}. 
	As claimed, the second isomorphism is induced by an isomorphism 
	\[
		\spec(\oo_{X,z}\shnsl)
		\isomorphism
		\spec(\oo_{\affl,0}\shnsl).
	\]
	To get it, it is crucial that $k$ is algebraically closed.
	Namely, if we take an open subset $V$ of $z$ in $X$ and étale map $f\mc V\to \affa_k^1$ (\coms why does this exist?, Apparently, this is SGAI, Thm. II.4.10\come), then $\kappa(z) = \kappa(f(z)) = k$ holds, and after post-composing with the translation, we can assume $f(z) = 0$.
	Now because $\kappa(z) = k$ holds, this implies that $f$ induces the desired isomorphism (\coms this needs more details, but i believe it for now\come).
	\par
	Continung with \emph{Step \ref{multscheme:affc:to-gal}}, we apply the long-exact excision sequence from \cref{multscheme:exc-seq} to $U_0 \defined \affl \setminus \lset 0 \rset$ and $Z = \lset 0 \rset$, which reads as 
	\[
		\begin{tikzcd}[column sep = small]
		0
		\ar{r}
		&
		\Gamma(\et{0},i^{!}\zz/n\zz)
		\ar{r}
		&
		\Gamma(\spec(\oo_{\affl,0}\shnsl),\zz/n\zz)
		\ar{r}
		\ar[draw = none]{d}[name = X, anchor = center]{}
		&
		\Gamma(U_0,\zz/n\zz)
		\ar[rounded corners,
	            to path={ -- ([xshift=2ex]\tikztostart.east)
	                      |- (X.center) \tikztonodes
	                      -| ([xshift=-2ex]\tikztotarget.west)
	                      -- (\tikztotarget)}]{dll}[at end]{}
		&
		\\
		&
		\hh_0^1(\spec(\oo_{\affl,0}\shnsl,\zz/n\zz))
		\ar{r}
		&
		\hh^1(\spec(\oo_{\affl,0}\shnsl,\zz/n\zz))
		\ar{r}
		\ar[draw = none]{d}[name = Y, anchor = center]{}
		&
		\hh^1_0(U_0,\zz/n\zz)
		\ar[rounded corners,
		            to path={ -- ([xshift=2ex]\tikztostart.east)
		                      |- (Y.center) \tikztonodes
		                      -| ([xshift=-2ex]\tikztotarget.west)
		                      -- (\tikztotarget)}]{dll}[at end]{}
		&
		\\
		&
		\hh_0^2(\spec(\oo_{\affl,0}\shnsl),\zz/n\zz)	
		\ar{r}
		&
		\hh^2(\spec(\oo\shnsl_{\affl,0}),\zz/n\zz)
		\ar{r}
		&
		\ldots	
		&
		\end{tikzcd}
	\]
	In it, the middle terms vanish for $i \geq 1$ (by \cref{comp:et-zar-shnsl},\ref{comp:et-zar-shnsl:vanish}), i.e. we have isomorphisms  
		\[
			\hh_0^i(U_0,\zz/n\zz)
			\isomorphism
			\hh_0^{i+1}(\spec(\oo_{\affl,0}\shnsl),\zz/n\zz)
		\]
		for $i\geq 1$, as well as 
		\[
			\hh_0^1(\spec(\oo_{\affl}\shnsl),\zz/n\zz)
			=
			\coker
			\left(
				\Gamma(\spec(\oo\shnsl_{\affl,0}),\zz/n\zz)
				\to 
				\Gamma(U_0,\zz/n\zz)
			\right).
		\]
		But the latter map \coms should be \come the identity (\coms it's induced by the restriction to $U_0$?\come), and hence $\hh^1_0(\spec(\oo_{\affl,0}\shnsl),\zz/n\zz) = 0$.
		To finish this step, we need to realize that $U_0$ is the spectrum of a field --- but since $\oo_{\affl,0}$ is a normal domain, its strict henselization is too, and so $U_0$ is its field of fraction, i.e. $U_0 = \spec(K)$, with $K \defined \fracfield(\oo_{\affl,0}\shnsl)$.
		The claimed isomoprhism 
		\[
			\hh_0^i(\spec(\oo_{\affl,0}\shnsl)\zz/n\zz)
			\cong 
			\hgal^{i-1}(\gal(K\sepclos/K),\zz/n\zz)
		\]
		comes from the general description of the étale cohomology of a point.
		\par
		Finally, we turn to \emph{Step \ref{multscheme:affc:galcomp}}.
		To calculate the absolute Galois group of $K$, we need the following fact (\cite{sga43}*{X.2.21}):
		%TODO: Add SGA to bibtex.
		\begin{fact}
			Let $(R,\idm)$ be a Henselian DVR with field of fractions $K$.
			Denote by $\widehat{R}$ the $\idm$-adically completion of $R$, and $\widehat{K} \defined \fracfield(\widehat{R})$.
			Then $\gal(K\sepclos/K) \cong \gal(\widehat{K}\sepclos/\widehat{K})$.
		\end{fact}
		To apply it, we note that $\oo_{\affl,0}\shnsl$ is given by the integral closure of $k[t,t^{-1}] \sse k\llbracket t \rrbracket $ and hence $\widehat{\fracfield(\oo_{\affl,0}\shnsl)} \cong k\laurent{t}$.
		It is now a general fact that $\gal(k\laurent{t}\sepclos,k\laurent{t}) \cong \widehat{\zz}$ holds for an algebraically closed field (the field $k\laurent{t}$ is ``quasi-finite'').
		A calculation of the group cohomology of $\widehat{\zz}$ with coefficients in $\zz/n\zz$ can be found in \cite{neu-coh}*{1.7.7}. 
		Roughly speaking, the cases $q = 1,2$ can be done by hand (and every finite $\widehat{\zz}$-module that has $n$-torsion).
		One then proceeds by induction, via the short-exact sequence 
		\begin{equation}
			\tag{$\ast$}
			\label{multscheme:affc:galcomp:ses}
			\begin{tikzcd}[column sep = small, cramped]
			0
			\ar{r}
			&
			\zz/n\zz
			\ar{r}
			&
			\Ind_{\widehat{\zz}}(\zz/n\zz)
			\ar{r}
			&
			(\zz/n\zz)_1
			\ar{r}
			&
			0
			\end{tikzcd},	
		\end{equation}
		where $\Ind_{\widehat{\zz}}(\zz/n\zz) = \hom_{\widehat{\zz}\text{-}\mathrm{cont.}}(\widehat{\zz},\zz/n\zz)$ and $(\zz/n\zz)_1$ is the cokernel of the map
		\[
			A
			\to 
			\Ind_{\widehat{\zz}}(\zz/n\zz)
			,
			~
			a
			\mapsto 
			\left[
				\tau \mapsto a
			\right].
		\]
		It can be shown (using explicit resolutions) that $\Ind_{\widehat{\zz}}(\zz/n\zz)$ has vanishing higher cohomology.
		Since $(\zz/n\zz)_1$ also has $n$-torsion, the long-exact cohomology sequence associated to \eqref{multscheme:affc:galcomp:ses} gives inductively the vanishing of the next-higher group cohomology.
		\par
		So we were finally able to compute the étale cohomology of $U_0$ with coefficients in $\zz/n\zz$, via the Galois cohomology of its field of rational functions.
		This concludes Step \ref{multscheme:affc:galcomp}, and since we've already spelled out how to finish the calculation from here, we're actually done.
\end{proof}


\begin{rem}
	The above proof actually works only if $\rchar(k) = 0$.
	In prime characteristic, the absolute Galois group of $k\laurent{t}$ isn't necessarily given by $\widehat{\zz}$.
	(This is supposed to be related to non-trivial ``Artin-Schreier coverings'', but I'm not sure what that is).
	However, the result still holds, we just need a slightly different approach to calculating the different cohomologies:
	\begin{itemize}
		\item
			We still have that $\hggal(k\laurent{t},\zz/n\zz) = 0$ for all $q \geq 2$.
			In fact, the field of Laurent polynomials over an algebraically closed field is always a so-called ``$C_1$-field'' (this is known as \emph{Tsen's theorem}, c.f. \cite{central-simple}*{Thm. 6.2.8}).
			It is now a general property of $C_1$-fields that their cohomological dimension is $\leq 1$ (\cite{central-simple}*{Prop. 6.2.3}).
		\item
			It still holds that $\hh^1(\etindex{U}{0},\zz/n\zz) \cong \zz/n\zz$ --- 
			we again get there via the Kummer sequence \eqref{etsheaves:kumm-sequence} (and an identification $\mu_n \cong \zz/n\zz$):
			The part of the long-exact cohomology sequence we're interested in reads as
			\[
				\begin{tikzcd}[column sep = small, cramped]
				0
				\ar{r}
				&
				\mu_n(k\laurent{t})
				\ar{r}
				&
				k\laurent{t}^{\times}
				\ar{r}[above]{(-)^{n}}
				&
				k\laurent{t}^{\times}
				\ar{r}
				&
				\hh^1(\etindex{U}{0},\mu_n)
				\ar{r}
				&
				\hh^1(\etindex{U}{0},\ox^{\times}) = 0,
				\end{tikzcd}
			\]	
			where the vanishing of $\hh^1(\etindex{U}{0},\ox^{\times})$ was already stated as \cref{multscheme:curve-gmult:galois} in the proof of \cref{multscheme:curve-gmult}.
			\coms This should imply it, but I'm missing something right now (it's been a long day).
			Alternatively, it seems to be possible to show this using the interpretation of $\hh^1$ in terms of torsors, but I don't understand what's going on in \cite{stacks}*{\stackstag{0A44}}. 
	\end{itemize}
\end{rem}
