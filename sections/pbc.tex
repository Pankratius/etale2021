\subsection{Proper base change}

\begin{numtext}
	\label{pbc:setup}
	Let $g'\mc X'\to X$ be a morphism of schemes, that sits in a cartesian diagram of the form
	\[
		\begin{tikzcd}
			X'
			\ar{r}[above]{g'}
			\ar{d}[left]{f'}
			\ar[phantom]{rd}{\lrcorner}
			&
			X
			\ar{d}[right]{f}
			\\
			S'
			\ar{r}[below]{g}
			&
			S
		\end{tikzcd}
	\]
	Let $\shf\in \shetcat{X}$ be a sheaf.
	The unit from the adjunction $\inverseimage{g'}\dashv \directimage{g'}$ gives a morphism $\shf \to \directimage{g'}\inverseimage{g'}\shf$ of sheaves on $\et{X}$.
	Pushing forward along $f$ now gives a morphism $\directimage{f}\to \directimage{g}\directimage{f'}\inverseimage{g'}\shf$ (where've already used the commutativity of the cartesian square and the fact that forming push-forwards is compatible with composition).
	Using the $\inverseimage{g'}\dashv \directimage{g'}$ adjunction once again, we end up with a morphism $\inverseimage{g}\directimage{f}\shf\to \directimage{f'}\inverseimage{g'}\shf$.
	Deriving everything, we end up with morphisms 
	\begin{equation}
		\label{pbc:morphism}
		\inverseimage{g}\rderived^i \directimage{f} \shf 
		\to 
		\rderived^i \directimage{f'}\inverseimage{g'}\shf
	\end{equation}
\end{numtext}

\begin{theorem}
	\label{pbc:theorem}
	Suppose we are in the situation of \cref{pbc:setup}.
	If $f\mc X\to S$ is proper and $\shf$ is a torsion sheaf on $\et{X}$, then the morphisms from \eqref{pbc:morphism} is an isomorphism for all $i\geq 0$:
	\[
		\inverseimage{g}\rderived^i \directimage{f} \shf 
		\isomorphism 
		\rderived^i \directimage{f'}\inverseimage{g'}\shf
	\]
	Moreover, for any complex $K^{\bullet} \in \pderivedd(\et{X})$, this extends to an isomorphism
	\[
		\inverseimage{g}\rderived \directimage{f} K^{\bullet} 
		\isomorphism
		\rderived \directimage{f'}\inverseimage{g'}K^{\bullet}
	\]
	in $\pderivedd(\et{S'})$.
\end{theorem}

\begin{numtext}
	The proof of this theorem will take us some time.
	We will do it in three steps:
	\begin{enumerate}
		\item 
			Show that the morphism $\inverseimage{g}\directimage{f}\shf \to \directimage{f'}\inverseimage{g'}\shf$ is an isomorphism.
			We will call this morphism the \emph{base change morphism}.
			This alone does not imply the theorem --- the functor $\inverseimage{g'}$ does not preserve injective objects, so we cannot simply pass to the derived functors.
		\item
			Reduce to morphisms $f$ of the form $\pp_S^1 \to S$.
		\item
			Show the theorem for morphisms of the form $\pp_S^1\to S$.
	\end{enumerate}
\end{numtext}

\subsubsection*{Step 1: The base change morphism is an isomorphism}
\begin{prop}[Stein factorization]
	Let $S$ be a scheme and $f\mc X\to S$ be a proper morphism.
	Then there exists a factorization
	\[
		\begin{tikzcd}
			X
			\ar{rr}[above]{f'}
			\ar{rd}[below left]{f}
			&
			&
			S'
			\ar{ld}[below right]{\pi}
			\\
			&
			S
			&
		\end{tikzcd}
	\]
	with the following properties:
	\begin{enumerate}
		\item 
			The morphism $f'$ is proper with geometrically connected fibres;
		\item
			The morphism $ss\pi$ is integral, if $S$ is locally noetherian, then it is finite;
		\item 
			It holds that $\directimage{f'}\ox = \oo_{S'}$;
		\item
			It holds that $S' = \underline{\spec}(\directimage{f}\ox)$; and
		\item 
			The scheme $S'$ is the normalization of $S$ in $X$.
	\end{enumerate}
\end{prop}
We will not prove this proposition, a reference is \cite{stacks}*{\stackstag{03GX}}.
\begin{defn}
	A \emph{Henselian pair} is a tuple $(A,I)$, where $A$ is a ring, $I \sse A$ is an ideal that satisfies $I \sse \rad(A)$ such that Hensel's Lemma holds for the projection $A[t] \twoheadrightarrow (A/I)[t]$.
\end{defn}

\begin{prop}
	\label{pbc:hnslpair-restrict}
	Let $(A,I)$ be a henselian pair, $f\mc X\to \spec(A)$ a proper morphism and denote by $X_0$ the following fiber product:
	\[
		\begin{tikzcd}
			X_0
			\ar[phantom]{rd}{\lrcorner}
			\ar{r}
			\ar{d}
			&
			X
			\ar{d}[right]{f}
			\\
			\spec(A/I)
			\ar{r}
			&
			\spec(A)
		\end{tikzcd}
	\]
	Then for any Zariski-sheaf $\shf \in \zar{X}$, it holds that $\Gamma(X,\shf) = \Gamma(X_0,\restrict{\shf}{X_0})$.
\end{prop}
Before we can show \cref{pbc:hnslpair-restrict}, we need the following lemma:
\begin{lem}
	Let $X$ be a topological space and $Z \sse X$ a closed subset.
	Assume:
	\begin{enumerate}
		\item
			$X$ is a spectral space, i.e. there's a ring $A$ such that $X \cong \abs{\spec(A)}$ holds as topological spaces; and 
		\item
			For every $x\in X$, the intersection $\overline{\lset x\rset} \cap Z$ is connected and non-empty.
	\end{enumerate}
	Then for any sheaf $\shf$ on $X$, it holds that $\Gamma(X,\shf) = \Gamma(Z,\restrict{\shf}{Z})$.
\end{lem}

\subsubsection*{Step 2: Reduction to morphisms of the form $\pp_S^1\to S$}

\begin{lem}
	Let $f\mc X\to S$ be a proper morphism.
	Then the following are equivalent:
	\begin{enumerate}
		\item
			Proper base change holds for $f$, i.e. the map from \eqref{pbc:morphism} is an isomorphism for all $i\geq 0$.
		\item 
			For all prime numbers $\ell$ and injective sheaves $\shi \in \shetcat{X}$ of $\zz/\ell\zz$-modules, the sheaf $\inverseimage{g'}\shi$ is $\directimage{f'}$-acyclic, i.e. for all $q\geq 1$, it holds that 
			\[
				\rderived^q \directimage{f'}\inverseimage{g'} \shi = 0.
			\]
	\end{enumerate}
\end{lem}

\begin{proof}
	If proper base change holds for $\shf$, then $\rderived^q \directimage{f'}\inverseimage{g'}\shi = \inverseimage{g}\rderived^q\directimage{f} \shi = 0$ for $q\geq 1$, since $\shi$ is injective, so it's in particular $\directimage{f}$-acyclic. Hence (i) $\Rightarrow$ (ii).
	\par 
	For the other direction, we start off with the following observation:
	\begin{fact}
		Let $\shf\in \shetcat{X}$ be a torsion sheaf.
		Then $\shf = \colim_{n} \shf[n]$, where 
		\[
			\shf[n]\defined \ker(\cdot n\mc \shf \to \shf)
		\]
		is the kernel of the ``multiplication by $n$''-map.
	\end{fact}
	
	Now all the functors we're considering commute with colimits (which comes from the various adjunctions they are part of), and hence it suffices to show the implication for sheaves of the form $\shf = \shf[n]$ with $n \in \zz$.
	Assume first that $n = \ell \cdot n'$ for some $n'\in \zz$.
	Then we have a short-exact sequence
	\[
		\begin{tikzcd}[column sep = small, cramped]
		0
		\ar{r}
		&
		\shf[\ell]
		\ar{r}
		&
		\shf
		\ar{r}
		&
		\shf/\shf[\ell]
		\ar{r}
		&
		0
		\end{tikzcd}.
	\]
	If proper base changes holds for both $\shf[\ell]$ and $\shf/\shf[\ell]$, then it holds for $\shf$ too:
	Indeed, by taking long-exact cohomology sequences, we get a commutative diagram of the form 
	\[
		\begin{tikzcd}[column sep = small, cramped]
		\inverseimage{g}\rderived^{q-1}\shf/\shf[\ell]  
		\ar{r}
		\ar{d}[right]{\cong}
		&
		\inverseimage{q}\rderived^{q}\directimage{f}\shf[\ell]
		\ar{r}
		\ar{d}[right]{\cong}
		&
		\inverseimage{g}\rderived^q\directimage{f}\shf
		\ar{r}
		\ar{d}
		&
		\inverseimage{g}\rderived^q\left(\shf/\shf[\ell]\right)
		\ar{r}
		\ar{d}[right]{\cong}
		&
		\inverseimage{g}\rderived^{q+1}\shf[\ell]
		\ar{d}[right]{\cong}
		\\
		\rderived^{q-1}\directimage{f'}\inverseimage{g'}\shf/\shf[\ell]
		\ar{r}
		&
		\rderived^q\directimage{f'}\inverseimage{g'}\shf[\ell]
		\ar{r}
		&
		\rderived^q\directimage{f'}\inverseimage{g'}\shf[\ell]
		\ar{r}
		&
		\rderived^q\directimage{f'}\inverseimage{g'}\left(\shf/\shf[\ell]\right)
		\ar{r}
		&
		\rderived^{q+1}\directimage{f'}\inverseimage{g'}\shf[\ell]
		\end{tikzcd}
	\]
	By the five-lemma, the middle map is an isomorphism too.
	Condinuing inductively until we exhaust all divisor of $n$, we may assume $n = \ell$.
	Let $\shi^{\bullet}$ by an injective resolution of $\shf$ in $\pderivedd(\et{X},\zz/\ell\zz)$.
	By the assumption of (ii), $\rderived^q\directimage{f'}\inverseimage{g'}\shi^q = 0$ for $q\geq 1$ and all $i$.
	So the $\inverseimage{g'}\shi^i$ are all $\directimage{f'}$-acyclic, and we can use them to compute the higher derived direct images:
	\begin{align*}
		\rderived^q \directimage{f'}\inverseimage{g'} \shf 
		&
		=
		\hh^q\left(\left(\directimage{f'}\inverseimage{g'}\shi^{\bullet}\right)\right)
		\\
		&
		=
		\hh^q\left(\inverseimage{g}\directimage{f}\shi^{\bullet}\right)
		\\
		&
		=
		\inverseimage{g}\rderived^q\directimage{f}\shf,
	\end{align*}
	where the second line uses the isomorphism from step 1, and the last line that $\inverseimage{g}$ is exact.
\end{proof}	

\begin{rem}
	Motivated by this lemma, we will fix a prime $\ell$ from now on and whenever we want to show that ``proper base change holds for a map'', we are going to show that (ii) of the lemma is satisfied for every injective sheaf $\shi$ of $\zz/\ell \zz$-module.
\end{rem}
\begin{prop}
	It suffices to prove \cref{pbc:theorem} for all morphisms of the form $\pp^1_S \to S$.
\end{prop}
To show this proposition, we are first going to establish
\begin{lem}
	Let $X\xrightarrow{f}Y\xrightarrow{h}S$ be proper morphisms.
	\begin{enumerate}
		\item
			If proper base change holds for $f$ and $h$, then it holds for $hf$ too.
		\item
			If $f$ is an epimorphism and proper base change holds for $f$ and $hf$, then it holds for $h$ too.
	\end{enumerate}
\end{lem}
\begin{proof}
	Consider a cartesian commutative square of the form 
	\[
		\begin{tikzcd}
		X' 
		\ar{r}[above]{f'}
		\ar{d}[left]{g''}
		&
		Y'
		\ar{r}[above]{h'}
		\ar{d}[right]{g'}
		&
		S'
		\ar{d}[right]{g}
		\\
		X
		\ar{r}[below]{f}
		&
		Y
		\ar{r}[below]{h}
		&
		S
		\end{tikzcd}
	\]
	\begin{enumerate}
		\item
		Let $\shi\in \shetcat{X}$ be an injective sheaf of $\zz/\ell\zz$-modules on $\et{X}$.
		Since proper base change holds for $f$, we have $\rderived^q\directimage{f'}\inverseimage{g''}\shi = 0$.
		Furthermore, $\directimage{f}$ preserves injective objects, and since proper base change holds for $h$ too, we also have $\rderived^q\directimage{h'}\inverseimage{g'}\left(\directimage{f}\shi\right) = 0$.
		We need to show $\rderived^q\directimage{\left(h'f'\right)}\left(\inverseimage{g''}\shi\right) = 0$.
		For that, we use the spectral sequence 
		\begin{equation}
			E_2^{p,q}
			=
			\rderived^p \directimage{h'} \rderived^q \directimage{f'}\left(\inverseimage{g''}\shi\right) 
			\Rightarrow
			\rderived^{p+q}\directimage{\left(h'f'\right)}\inverseimage{g''}\shi.
		\end{equation}
		Since $\rderived^q\directimage{f'}\left(\inverseimage{g''}\shi\right) = 0$ for $q\geq 1$, only the $q  = 0$ row remains, and hence 
		\[
			\rderived^p \directimage{h'}\directimage{f'} \left(\inverseimage{g''}\shi\right) \cong \rderived^p\directimage{\left(h'f'\right)}\inverseimage{g''}\shi.
		\]
		By step 1, we have
		\[
			\rderived^p\directimage{h'}\left(\directimage{f'}\inverseimage{g''}\shi\right)
			=
			\rderived^p\directimage{h'}\left(\inverseimage{g'}\directimage{f}\shi\right);
		\]
		and we've already argued at the start of the proof of this claim that the right hand side vanishes.
		\item 
		Let $\shj \in \shetcat{Y}$ be an injective sheaf; we have to show that $\rderived^q\directimage{h'}\inverseimage{g'} \shj = 0$ for all $q \geq 1$.
		Choose an embedding $\inverseimage{f}\shj \hookrightarrow \shj'$, where $\shj'$ is an injective sheaf of $\zz/\ell\zz$-modules on $\et{X}$.
		\footnote{I'm not entirely sure if it is automatic that we can do this, but $\zz/\ell\zz\modcat$ has enough injectives, and since arbitrary product of injective objects are injective we should just be able to redo the construction of \cref{chlgy:enough-inj} to get the injetive object} 
		Under the adjunction $\inverseimage{f} \dashv \directimage{f}$, this corresponds to a map $\pphi\mc \shj \to \directimage{f}\shj'$.
		\begin{claim}
			The map $\pphi$ is injective.
		\end{claim}
		\begin{claimproof}
			It suffices to check this on stalks of geometric points.
			So let $\overline{y}\to Y$ be a geometric point of $Y$, with underlying point $y$.
			Since $f\mc X \to Y$ is surjective, we can find a $x\in X$ such that $f(x) = y$.
			
		\end{claimproof}
	\end{enumerate}
\end{proof}



\subsubsection*{Some corollaries}

\begin{cor}
	Let $\overline{s} \to S$ be a geometric point, $f\mc X \to S$ a proper morphism and $\shf \in \shetcat{X}$ a torsion sheaf.
	Then for all $q \geq 0$, it holds that 
	\[
		\left(\rderived^q \directimage{f} \shf\right)_{\overline{s}}
		\cong
		\hh^q\left(X_{\overline{s}},\restrict{\shf}{X_{\overline{s}}}\right)
	.\] 
	If every fiber of $f$ has dimension $\leq n$, then $\rderived^q \directimage{f} \shf = 0$ for all $q > 2n$.
\end{cor}

\begin{proof}
	The first statement is PBC applied to the the fiber square 
	\[
		\begin{tikzcd}
			X_{\overline{s}}
		\ar{r}
		\ar{d}
		\ar[phantom]{rd}{\lrcorner}
		&
		X
		\ar{d}
		\\
		\kappa(\overline{s})
		\ar{r}
		&
		S
		\end{tikzcd}.
	\]
	The second follows from applying \cref{coh-dim:main-theorem} to $\hh^q\left(X_{\overline{s}},\restrict{\shf}{X_{\overline{s}}}\right)$, since $\kappa(\overline{s})$ is separably closed.
\end{proof}
