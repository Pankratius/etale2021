\section{Smooth base change}

\subsection{Statement of the theorem}
\begin{theorem}
	Consider a Cartesian square
	\[
		\begin{tikzcd}
		X'
		\ar{r}[above]{g'}
		\ar{d}[left]{f'}
		\ar[phantom]{rd}{\lrcorner}
		&
		X
		\ar{d}[right]{f}
		\\	
		S'
		\ar{r}[below]{g}
		&
		S
		\end{tikzcd}
	\]
	where $S = \lim S_{\lambda}$ with smooth $g_{\lambda}\mc S_{\lambda} \to S$ and affine transition maps $S_{\lambda} \to S_{\lambda'}$.
	Suppose further that $f$ is quasi-compact and quasi-separated.
	Then, for any torsion abelian sheaf $\shf$ on $\et{X}$ which torsion orders invertible in $S$, the base-change map 
	\[
		\inverseimage{g} \rderived^q \directimage{f} \shf 
		\longrightarrow
		\rderived^q \directimage{f'} \left(\inverseimage{g'} \shf\right)
	\]
	is an isomorphism for all $q \geq 0$.
\end{theorem}

\subsection{Cohomology of $\affa^d_k$ and $\projp^d_k$}

\begin{prop}
	Let $k$ be a separably closed field, $d \geq 1$.
	Let $n \neq \rchar k$ be a positive integer.
	Then 
	\[
		\hh^j(\affa_k^d,\zz/n\zz)
		\cong
		\begin{cases}
			\zz/n\zz
			&
			\text{if $j = 0$;}
			\\
			0
			&
			\text{otherwise.}
		\end{cases}
	\] 
\end{prop}

\begin{proof}
	We proceed by induction, and use a Leray spectral sequence for the inductive step.
	The base case $n = 1$ is given implied by \cref{multscheme:curve-gmult}:
	After base changing to an algebraic closure of $k$, we have the short exact sequence 
	\[
		\begin{tikzcd}[column sep = small, cramped]
		0
		\ar{r}
		&
		\underline{\zz/n\zz}
		\ar{r}
		&
		\gmult
		\ar{r}
		&
		\gmult
		\ar{r}
		&
		0
		\end{tikzcd}.	
	\]
	Now by the theorem, $\hh^j(\affa_k^1,\gmult) = 0$ for all $j \geq 1$ (since $\affa_k^1$ admits no non-trivial line bundles, i.e. its Picard group vanishe).
	Taking the associated long-exact cohomology sequence to the above short-exact sequence implies the result
	(Note that for $j = 0$, we have $\hh^0(\affa_k^1,\zz/n\zz) = \Gamma(\affa_k^1,\zz/n\zz) = \zz/n\zz$).
	\par
	For the general case, we apply smooth base change to the Cartesian diagram:
	\[
		\begin{tikzcd}
		\affa_k^{d-1} \times \affa_k^1
		\ar{r}
		\ar{d}[left]{\pi}
		\ar[phantom]{rd}{\lrcorner}
		&
		\affa_1
		\ar{d}[right]{g}
		\\
		\affa_k^{d-1}
		\ar{r}
		&
		\spec(k)
		\end{tikzcd}
	\]
	Accordingly, we have 
	\[
		\rderived^q \directimage{\pi}\underline{\zz/n\zz}
		\cong
		\inverseimage{f}\rderived^q\directimage{g}\underline{\zz/n\zz}
	.\] 
	Now if $q = 0$, this equals $\underline{\zz/n\zz}$, and if $q\geq 1$, then it vanishes (by the base case of the induction).
	To conclude the proof, we apply the Leray spectral sequence:
	\[
		E_2^{p,q}
		=
		\rderived^p\Gamma(\affa^{d-1}_k,\rderived^{q}\directimage{g}\underline{\zz/n\zz})
		\Rightarrow
		\rderived^{p+q}\Gamma(\affa_k^{d},\underline{\zz/n\zz})
	\]
	By the above consideration, this degenarates, and hence 
	\[
		\rderived^p\Gamma(\affa_k^{d-1},\zz/n\zz)
		\cong
		\rderived^p\Gamma(\affa_k^d,\zz/n\zz).
	\]
\end{proof}

\begin{prop}
	Let $k$ be a separably closed field, $d \geq 1$.
	Let $n \neq \rchar k$ be a positive integer.
	Then 
	\[
		\hh^j_c(\affa_k^d,\zz/n\zz)
		\cong
		\begin{cases}
			\zz/n\zz
			&
			\text{if $j = 2d$;}
			\\
			0
			&
			\text{otherwise;}
		\end{cases}
	\]
	where $\hh_c^j(\affa_k^d,-)$ denotes the étale cohomology of $\affa_k^d$ with compact support.
\end{prop}

\begin{prop}
\label{sbc:coh-wsup-of-affine-space}
	Let $k$ be a separably closed field, $d\geq 1$.
	Let $n \neq \rchar k$ be a positive integer.
	Then 
	\[
		\hh^j(\pp^d_k,\zz/n\zz)
		\cong
		\begin{cases}
			\zz/n\zz
			&
			\text{if $0 \leq j \leq 2d$ and $j$ is even;}
			\\
			0
			&
			\text{otherwise.}
		\end{cases}
	\]
\end{prop}

\begin{proof}
	Again we proceed by induction:
	The base case \coms is outsourced. \come
	%TODO: Add cohomology of smooth projective curves.
	Consider the open-closed decomposition $\begin{tikzcd}[column sep = small, cramped] \pp_k^{d-1} \ar[hook]{r}[above]{i}&\pp_k^d \ar[hookleftarrow]{r}[above]{j}& \affa_k^d\end{tikzcd}$.
	Associated to it, we have the short-exact sequence (\cite{stacks}*{\stackstag{095L}}) 
	\begin{equation}
		\label{sbc:excision-sequence}
		\tag{$\ast$}
		\begin{tikzcd}[column sep = small, cramped]
		0
		\ar{r}
		&
		j_{!}\inverseimage{j} \underline{\zz/n\zz}
		\ar{r}
		&
		\underline{\zz/n\zz}
		\ar{r}
		&
		\directimage{i}\inverseimage{i}\underline{\zz/n\zz}
		\ar{r}
		&
		0
		\end{tikzcd}
	\end{equation}
	of abelian sheaves on $\etindex{\pp^d}{k}$.
	We have $\hh^p(\etindex{\pp^d}{k},j_{!}\inverseimage{j}\zz/n\zz) \cong \hh_c^p(\etindex{\affa^d}{k},\zz/n\zz)$, and know the latter cohomology groups by \cref{sbc:coh-wsup-of-affine-space}.
	Furthermore, since $\underline{\zz/n\zz}$ is constant, we have $\hh^p(\etindex{\pp^{d-1}}{k},\zz) \cong \hh^p(\etindex{\pp^{d-1}},\directimage{i}\inverseimage{i}\underline{\zz/n\zz})$.
	So the long-exact cohomology sequence associated to \cref{cbs:excision-sequence} gives for $p \leq 2d-2$:
	\[
		\hh^p(\etindex{\pp^d}{k},\underline{\zz/n\zz})
		\cong
		\hh^{p}(\etindex{\pp^{d-1}}{k},\underline{\zz/n\zz}).
	\]
	For the top degrees, it is given by 
	\[
		\begin{tikzcd}[column sep = small, cramped]
		\ldots
		\ar{r}
		&
		\hh^{2d}_c(\etindex{\affa^d}{k},\underline{\zz/n\zz})
		\ar{r}
		&
		\hh^{2d}(\etindex{\pp^d}{k},\underline{\zz/n\zz})
		\ar{r}
		&
		\hh^{2d}(\etindex{\pp^{d-1}}{k},\underline{\zz/n\zz})
		\ar{r}
		&
		\ldots	
		\end{tikzcd},
	\]
	and hence $\hh^{2d}(\etindex{\pp^d}{k},\underline{\zz/n\nn}) \cong \hh^{2d}_c(\etindex{\affa^d}{k},\underline{\zz/n\zz}) \cong \zz/n\zz$.
	\par
	Finally, for $p = 2d-1$, we have 
	\[
		\begin{tikzcd}[column sep = small, cramped]
		\ldots
		\ar{r}
		&
		\hh^{2d-1}_c(\etindex{\affa^d}{k},\underline{\zz/n\zz})
		\ar{r}
		&
		\hh^{2d-1}(\etindex{\pp^d}{k},\underline{\zz/n\zz})
		\ar{r}
		&
		\hh^{2d-1}(\etindex{\pp^{d-1}}{k},\underline{\zz/n\zz})
		\ar{r}
		&
		\ldots	
		\end{tikzcd},
	\]
	and both the left and the right term vanish, so $\hh^{2d}(\etindex{\pp^d}{k},\underline{\zz/n\nn}) = 0$. 
\end{proof}
